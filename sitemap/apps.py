from django.apps import AppConfig


class SitemapConfig(AppConfig):
	name = 'sitemap'

	def ready(self):

		from .base import Base

		Base().create_sitemap()