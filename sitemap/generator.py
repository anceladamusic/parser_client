import xml.etree.ElementTree as ET



def generate_sitemap(data):
    urlset_tag = ET.Element('urlset', attrib = {
        'xmlns' : 'http://www.sitemaps.org/schemas/sitemap/0.9',
        'xmlns:image' : 'http://www.google.com/schemas/sitemap-image/1.1'
        }
    )

    for page in data.keys():
        url_tag = ET.SubElement(urlset_tag, 'url')
        ET.SubElement(url_tag, 'loc').text = page

        for image in data[page]:
            image_tag = ET.SubElement(url_tag, 'image:image')
            ET.SubElement(image_tag, 'image:loc').text=image

    tree = ET.ElementTree(urlset_tag)
    tree.write('sitemap/templates/sitemap.xml', xml_declaration=True, encoding='utf-8')



# DATA EXAMPLE
#
# {
#     'http://example.com/index.html' : [
#         'http://example.com/images/1.png',
#         'http://example.com/images/2.png'
#         ],
#     'http://example.com/account.html' : [
#         'http://example.com/images/3.png',
#         'http://example.com/images/4.png']
# }