from django.conf.urls import url
from django.views.generic.base import TemplateView
from . import views

urlpatterns = [
    url(r'^sitemap/create/$', views.create),
    url(r'^sitemap\.xml$', TemplateView.as_view(template_name='sitemap.xml', content_type="text/plain")),
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type="text/plain")),
]