from django.http import HttpResponse
from django.shortcuts import render
from .base import Base

# Create your views here.
def create(request):

	result = Base().create_sitemap()

	return HttpResponse(result)