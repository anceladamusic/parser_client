from django.apps import apps
from .generator import generate_sitemap

class Base():

	def __init__(self):
		self.domain = 'http://news-mining.online'
		self.exclude = [
		]

	def create_sitemap(self):

		models = self.__get_models_with_patterns()

		result = {}

		line = self.domain

		result[line] = []

		for model in models:
			
			elems = model.objects.all().values('id', 'level')

			class_name = model().get_class_name()

			for elem in elems:

				if class_name not in self.exclude:
					
					line = '{0}/{1}/{2}/'.format(self.domain, class_name, elem['id'])

				result[line] = []

		generate_sitemap(result)


	def __get_models_with_patterns(self):

		models = apps.get_models()

		return [model for model in models if getattr(model, 'get_class_name', None)]