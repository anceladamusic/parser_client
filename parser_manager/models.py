from uuid import uuid1
from collections import namedtuple

from django_mysql.models import Model
from django.db import models

from mptt.models import MPTTModel, TreeForeignKey

class Agency(MPTTModel):
	class Meta():
		verbose_name = 'информационное агентство'
		verbose_name_plural = 'информационные агентства'
		db_table = 'agency_agency'

	id = models.UUIDField(default=uuid1, editable=False, db_index=True, primary_key=True)
	name = models.CharField('наименование', max_length=200, blank=True, null=True)
	url = models.CharField('ссылка на главную', max_length=200, blank=True, null=True)
	parent = TreeForeignKey(
		'self', null=True, blank=True, related_name='children', db_index=True, verbose_name='родитель', \
		on_delete=models.CASCADE)

	def __str__(self):
		return '{0}'.format(self.name)

	def as_dict(self):
		if self.parent:
			parent = self.parent.as_dict()

		else:
			parent = None

		return {
			'name': self.name,
			'url': self.url,
			'parent': parent,
		}

class Thematic(models.Model):
	class Meta():
		verbose_name = 'тематика'
		verbose_name_plural = 'тематики'
		db_table = 'agency_thematic'

	id = models.UUIDField(default=uuid1, editable=False, db_index=True, primary_key=True)
	name = models.CharField('наименование', max_length=200, blank=True, null=True)

	def __str__(self):
		return self.name

	def as_dict(self):
		return {
			'id': str(self.id),
			'name': self.name,
		}

class ThematicAgency(models.Model):
	class Meta():
		verbose_name = 'связка тематика-агентство'
		verbose_name_plural = 'связки тематика-агентство'
		db_table = 'agency_thematicagency'

	Agency = models.ForeignKey(Agency, on_delete=models.CASCADE)
	Thematic = models.ForeignKey(Thematic, on_delete=models.CASCADE)
	db_table = 'agency_thematicagency'

	def __str__(self):
		return '{0} | {1}'.format(Agency, Thematic)

	def as_dict(self):
		return getattr(self, 'Thematic').as_dict()

# сборщик
class Crawler(MPTTModel):

	c_type = namedtuple('type', ['name', 'value'])

	SITE = c_type('site', 'сайт')
	VK = c_type('vk', 'вконтакте')

	TYPE_CHOICES = (
		SITE,
		VK,
	)

	class Meta():
		verbose_name = 'Программа сборщик'
		verbose_name_plural = 'Программы сборщики новостей'
		db_table = 'crawler_crawler'

	id = models.UUIDField(default=uuid1, editable=False, db_index=True, primary_key=True)
	file = models.FileField('файл', null=True)
	Agency = TreeForeignKey(Agency, on_delete=models.CASCADE, null=True)
	type = models.CharField('тип сборщика', max_length=200, choices=TYPE_CHOICES, default=SITE.value)
	parent = TreeForeignKey(
		'self', null=True, blank=True, related_name='children', db_index=True, verbose_name='родитель', \
		on_delete=models.CASCADE)

	def __str__(self):
		return '{0}'.format(self.Agency)

	def as_dict(self):

		return {
			'id': str(self.id),
			'type': self.type,
			'parent': self.parent_id,
			'agency': self.Agency.as_dict(),
			'thematic': [
				thematic.as_dict() for thematic in ThematicAgency.objects.using(
					'manager').filter(Agency_id=self.Agency)],
		}