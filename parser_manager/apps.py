from django.apps import AppConfig


class ParserConfig(AppConfig):
    
    name = 'parser_manager'

    def ready(self):

    	from .base import Base

    	Base().build_agency()