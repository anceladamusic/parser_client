import json



def to_jsonld(google_objects):
    result = []

    for item in google_objects:
        attributes = [a for a in dir(item) if not a.startswith('__') and not callable(getattr(item,a))]
        to_json = {
            '@context' : 'http://schema.org',
        }

        for attr in attributes:
            if attr == 'type':
                to_json['@' + attr] = getattr(item, attr)
            else:
                to_json[attr] = getattr(item, attr)
        result.append(json.dumps(to_json))

    return result