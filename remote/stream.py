class Streams():

	def __init__(self):

		self.stream = []

	def create_stream(self, key):

		stream = Stream(key)

		self.stream.append(stream)

		return stream

	def as_dict(self):
		return {
			'stream': [stream.as_dict() for stream in self.stream],
		}


class Stream():

	def __init__(self, key):
		self.uuid = key
		self.elements = []


	def add_element(self, func, **kwargs):

		self.elements = func(**kwargs)

	def as_dict(self):
		return {
			'uuid': str(self.uuid),
			'elements': self.elements,
		}