import ipdb
import datetime
import redis
import pickle

from remote.remote import Remote
from mainapp.base import Base as MainappBase

def get_cache_table(name):

	stream_object = Remote().get_stream(name)
	
	return stream_object.elements

class Cache():

	def __init__(self, **kwargs):

		self.r = redis.StrictRedis('localhost')

		self.days = kwargs['days']
		self.name = kwargs['name']
		self.table_with_objects = kwargs['table_with_objects']
		self.fields = kwargs['fields']
		self.query_field = kwargs['query_field']
		self.keys = self.__get_keys
		self.cache_max_days = 60
	
		
	# все ключи для таблицы
	def __get_keys(self):

		date_ranges = self.__get_ranges(self.days*3)

		keys = self.r.keys()

		result = []

		for date_range in date_ranges:

			if pickle.dumps({'name': self.name, 'packet': date_range.date()}
				) in keys:

				result +=[{'name': self.name, 'packet': date_range.date()}]

		return result

	def __get_ranges(self, days):

		now = datetime.datetime.now()

		end_date = MainappBase().datetime_localize(datetime.datetime(
			day=now.day, month=now.month, year=now.year))

		start_date = MainappBase().datetime_localize(datetime.datetime(day=1, month=1, year=2018))

		return MainappBase().daterange(**{
			'start_date': start_date,
			'end_date':	end_date,
		})

	# сохраняем конкретный день
	def __redis_save(self, date_range):

		queryset = self.table_with_objects.filter(**{
				'{0}__range'.format(self.query_field): (
					date_range,
					date_range + datetime.timedelta(days=1) - datetime.timedelta(seconds=1)
				),
			}).order_by('date').values(*self.fields)

		key = pickle.dumps({
			'name': self.name,
			'packet': date_range.date(),
		})

		self.r.delete(key)

		self.r.set(key, pickle.dumps(list(queryset)))

		self.r.save()

		print ({
			'name': self.name,
			'packet': date_range.date(),
		})

	# создаем новую таблицу
	def create_cache_table(self):

		date_ranges = self.__get_ranges(self.cache_max_days)

		for date_range in date_ranges:

			self.__redis_save(date_range)


	# удалить таблицу из кеш
	def delete_cache_table(self):

		for key in self.keys():

			self.r.delete(pickle.dumps(key))

	# обновляем таблицу
	def actualize(self, days):
		
		# self.delete_cache_table()

		# обновляем таблицы
		if not self.keys():

			print ('------------start createing cache table')
			self.create_cache_table()

		date_ranges = self.__get_ranges(self.cache_max_days*2)

		date_ranges_current = self.__get_ranges(self.cache_max_days)

		not_in_current = [
			date_range for date_range in date_ranges if not date_range in date_ranges_current]

		# удаляем
		for date_range in not_in_current:

			key = pickle.dumps({
				'name': self.name,
				'packet': date_range.date(),				
			})

			in_redis = self.r.get(key)

			if in_redis:

				self.r.delete(key)
				
		# добавляем отсутствующие
		for date_range in date_ranges_current:

			key = {
				'name': self.name,
				'packet': date_range.date(),
			}

			in_redis = self.r.get(pickle.dumps(key))

			if not in_redis:

				self.__redis_save(date_range)
				
		# обноволяем последние
		[self.__redis_save(date_ranges[-i]) for i in range(0, days)]

		print ('------------------end update_pubs')