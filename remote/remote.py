import time
import uuid
import threading

from .stream import Streams

streams = []

class Remote():

	def __init__(self):
		
		self.streams = streams

		self.streams_count = 0

	def get_stream(self, key):

		if self.streams:

			return [stream for stream in self.streams[0].stream if stream.uuid == key][0]
		

	def remove_stream(self, key):
		if streams:

			stream = [stream for stream in streams[0].stream if stream.uuid == key]

			if stream:

				streams[0].stream.remove(stream[0])

	def get_streams(self):

		return streams

class MainLoop(Remote):

	def __init__(self, **kwargs):
		self.func = kwargs['func']
		self.func_args= kwargs['func_args']
		self.timeout = kwargs['timeout']

	def run(self):

		while True:

			if self.func_args:

				self.func(**self.func_args)

			else:
				
				self.func()

			time.sleep(self.timeout)

	def start(self, **kwargs):

		self.status = 'played'

		thread = threading.Thread(target=self.run)

		thread.start()

	def stop(self):

		self.status = 'stop'

		self.process.terminate()

		self.pid = 0