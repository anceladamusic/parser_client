import os
import sys
import fcntl
import ipdb

from django.apps import AppConfig

from .cache import get_cache_table

class RemoteConfig(AppConfig):
	
	name = 'remote'
	
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	
	pid_file = os.path.join(BASE_DIR, 'program.pid')
	
	fp = open(pid_file, 'w')
	
	# try:
	# 
	# 	fcntl.lockf(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
	# 
	# except IOError:
	# 
	# 	sys.exit(0)
		
	def ready(self):
		
		print ('----------run')
		
		from .remote import Remote
		from .stream import Streams
		from .cache import Cache
		from .remote import MainLoop
		from message_queue.message import Message 
		from mainapp.base import Base as MainappBase
		from canonizator.models import NormalizePublication
		from parser_manager.models import Crawler
	
		streams = Remote().get_streams()
	
		# создаем объект управления
		streams.append(Streams())
		
		######################
		# active_users
		######################
	
		# добавляем поток
		streams[0].create_stream('active_users')
	
		# вытаскиваем поток
		active_users_stream = Remote().get_stream('active_users')
	
		active_users_stream.elements = []
	
		######################
		# message redis_connection
		######################
	
		# добавляем поток
		streams[0].create_stream('messages')
	
		# вытаскиваем поток
		messages_stream = Remote().get_stream('messages')
	
		messages_stream.add_element(Message)
	
		########################
		# cache pubs
		########################
	
		# добавляем поток
		streams[0].create_stream('pubs')
	
		# вытаскиваем поток
		cache_pubs_stream = Remote().get_stream('pubs')
	
		# добавляем в кеш таблицу NormalizePublication
		cache_pubs_stream.add_element(Cache, **{
			'days': 30,
			'name': 'pubs',
			'table_with_objects': NormalizePublication.objects.using('canonizator'),
			'fields': [
				'id',
				'title_poses',
				'text_poses',
				'status_title',
				'status_text',
				'date',
			],
			'query_field': 'date',
		})
		
		#########################
		# agencys
		#########################
		
		# добавляем поток
		streams[0].create_stream('agencys')
		
		# вытаскиваем поток
		agencys_stream = Remote().get_stream('agencys')
		
		agencys_stream.elements = [crawler.as_dict() for crawler in Crawler.objects.using('manager').all()]