import ipdb
from remote.remote import Remote

def get_active_users():

	active_users = Remote().get_stream('active_users')
	
	if active_users:
		
		return active_users.elements

def remove_from_active_users(user_id, page_uuid, session_key):

	active_users = get_active_users()

	active_user = [active_user for active_user in active_users if active_user['id'] == user_id]

	if active_user:

		session = [session for session in active_user[0]['sessions'] if session['key'] == session_key]

		if session:

			page = [page for page in session[0]['pages'] if str(page['uuid']) == page_uuid]

			if page:

				session[0]['pages'].remove(page[0])

				if not session[0]['pages']:

					active_user[0]['sessions'].remove(session[0])

		if not active_user[0]['sessions']:

			active_users.remove(active_user[0])

def get_page(user_id, session_key, page_uuid):
	
	active_users = get_active_users()

	active_user = [active_user for active_user in active_users if active_user['id'] == user_id]
	
	if active_user:
	
		session = [session for session in active_user[0]['sessions'] if session['key'] == session_key]
	
		if session:
	
			page = [page for page in session[0]['pages'] if str(page['uuid']) == page_uuid]
	
			if page:
	
				return page[0]

class ActiveUsers():

	def __init__(self, **kwargs):
		self.active_users = get_active_users()
		self.page_type = kwargs['page_type']
		self.page_uuid = kwargs['page_uuid']
		self.can_receive_data = kwargs['can_receive_data']
		if 'data' in kwargs:
			self.data = kwargs['data']
		else:
			self.data = []

	# создаем или обновляем сессию
	def update(self, request):

		if 'client_id' in request.session:

			user_id = request.session['client_id']

			session_key = request.session.session_key

			active_user = [
				active_user for active_user in self.active_users if active_user['id'] == user_id]

			if not active_user:

				active_user = [{
					'id': user_id,
					'sessions': [],
				}]

				self.active_users += active_user

			session = [session for session in active_user[0]['sessions'] if session['key'] == session_key]

			if session:

				page = [page for page in session[0]['pages'] if str(page['uuid']) == str(
					self.page_uuid)]

				if page:

					page[0]['can_receive_data'] = self.can_receive_data

				else:

					session[0]['pages']+=[{
						'type': self.page_type,
						'uuid': self.page_uuid,
						'data': self.data,
						'can_receive_data': self.can_receive_data,
					}]

			else:

				page = {
					'type': self.page_type,
					'uuid': self.page_uuid,
					'data': self.data,
					'can_receive_data': self.can_receive_data,
				}

				active_user[0]['sessions'] += [{
					'key': session_key,
					'pages': [page]
				}]