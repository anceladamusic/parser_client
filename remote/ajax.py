import json
from django.http import JsonResponse

from .remote import Remote
from .remote import MainLoop
from .stream import Streams
from .cache import get_cache_table

class Ajax():

	def __init__(self):
		pass

	def read_ajax(self, request):

		if request.method == 'POST':

			string = json.loads(request.body.decode('utf-8'))
			
			if string['method'] == 'run_remote':
				
				streams = Remote().get_streams()

				####################
				# cache pubs
				####################

				cache_table = get_cache_table('pubs')

				######################
				# функции
				######################

				# обновляем таблицу pubs за последний день
				kwargs = {
					'func': cache_table.actualize,
					'func_args': {
						'days': 1,
					},
					'timeout': 600,
				}

				MainLoop(**kwargs).start()
				
				# обновляем таблицу pubs за последние 7 дней
				kwargs = {
					'func': cache_table.actualize,
					'func_args': {
						'days': 7,
					},
					'timeout': 600,
				}

				MainLoop(**kwargs).start()

				return JsonResponse({
					'result': 'ok',	
				})