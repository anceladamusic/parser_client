from uuid import uuid1

from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template.loader import render_to_string

from mainapp.base import Base as MainappBase

def remote(request):

	session = MainappBase().check_authorize(request)

	if not session['client_id']:

		return HttpResponseRedirect(reverse('mainpage:index'))

	page_uuid = uuid1()

	content = render_to_string('remote.html')

	return render(request, 'main.html', {
		'page_uuid': page_uuid,
		'page_type': 'remote',
		'content': content,
		'session': session,
		'session_key': request.session.session_key,
	})