from mptt.models import MPTTModel, TreeForeignKey
from django.db import models

class AbstractTop(MPTTModel):

	class Meta:
		abstract = True

	name = models.CharField(max_length=400, verbose_name='Наименование', null=True)
	description = models.TextField(null=True, blank=True, verbose_name='Описание')
	short_description = models.TextField(null=True, blank=True, verbose_name='Краткое описание')
	parent = TreeForeignKey('self', null=True, blank=True, \
	 related_name='children', db_index=True, on_delete=models.CASCADE)
	topbar_category = models.CharField(
		'Параметр раздела в ссылке', max_length=100, null=True, blank=True)

	def __str__(self):
		return '{0}'.format(self.name)

	def get_type():
		return 'MPTTModel'