from django.template.loader import render_to_string

from oauth.properties import Properties

class Common(Properties):

	def __init__(self):
		super().__init__()
		self.prop = self.auth_settings

	def build_login_panel(self, request):
		return render_to_string('common/login_panel.html', {
			'vk': self.prop['vk'],
			'fb': self.prop['fb'],
			'gp': self.prop['gp'],
			'ya': self.prop['ya'],
			'request': request,
		})

	def build_elements(self, element_html, elements, selected):

		result = []

		for element in elements:

			selected_mark = False

			if selected:

				if element['id'] == selected['id']:
					selected_mark = True

			result.append(render_to_string(element_html, {
				'element': element,
				'selected': selected_mark,	
			}))

		return result

class Topbar():

	def __init__(self, **kwargs):
		self.request = kwargs['request']
		self.element_html = kwargs['element_html']
		self.elements = kwargs['elements']
		self.selected = kwargs['selected']

	

	def build(self):

		topbar_elements = Common().build_elements(**{
			'element_html': self.element_html,
			'elements': self.elements,
			'selected': self.selected
		})

		return render_to_string('mainpage/topbar.html', {
			'login_panel': Common().build_login_panel(self.request),
			'topbar_elements': topbar_elements,
		})

class OauthTopbar():

	def __init__(self, **kwargs):
		self.request = kwargs['request']
		self.element_html = kwargs['element_html']
		self.elements = kwargs['elements']
		self.selected = kwargs['selected']
		self.campaign_name = kwargs['campaign_name']
		self.notifications = kwargs['notifications']


	def build(self):

		topbar_elements = Common().build_elements(**{
			'element_html': self.element_html,
			'elements': self.elements,
			'selected': self.selected,	
		})

		return render_to_string('campaign/topbar.html', {
			'login_panel': Common().build_login_panel(self.request),
			'campaign_name': self.campaign_name,
			'topbar_elements': topbar_elements,
		})