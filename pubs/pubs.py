import ipdb
import asyncio

from django.template.loader import render_to_string

from remote.agencys import get_agencys
from canonizator.models import NormalizePublication

from .pub import Pub

class Pubs():
    
    def __init__(self, **kwargs):
        self.keywords_lists = kwargs['keywords_lists']
        self.pubs = kwargs['pubs']
        self.agencys = get_agencys()
        self.fields = [
            'id',
            'title',
            'date',
            'crawler_id',
            'url',
            'status_text',
            'title_poses',
            'text_poses',
        ]
        self.queryset_result = NormalizePublication.objects.using('canonizator').filter(
            id__in=[pub['id'] for pub in self.pubs]).values(*self.fields)
        # результат обработки
        self.pubs_objects = self.__init_pub_objects()
        self.html = None
        
    def __init_pub_objects(self):
        
        result = []
        
        for pub in self.queryset_result:
            
            result.append(
                Pub(**{
                    'pub': pub,
                    'keywords_lists': self.keywords_lists,
                    'agencys': self.agencys,
                })
            )
        
        return result
    
    def __fill_html_with_pubs(self):
        
        self.html = render_to_string('pubs.html', {'pubs': [pub.html for pub in self.pubs_objects]})
    
    def run(self):
        
        loop = asyncio.new_event_loop()

        asyncio.set_event_loop(loop)
        
        tasks = [loop.create_task(pub.run()) for pub in self.pubs_objects]
        
        loop.run_until_complete(asyncio.wait(tasks))
        
        loop.close()
        
        # делаем табицу из объектов html
        self.__fill_html_with_pubs()