import uuid

from django.template.loader import render_to_string

class Pub():
    
    def __init__(self, **kwargs):
        self.pub = kwargs['pub']
        self.keywords_lists = kwargs['keywords_lists']
        self.agencys = kwargs['agencys']
        self.search_fields = ['title_poses', 'text_poses']
        
        self.title = self.pub['title']
        self.date = self.pub['date']
        self.agency = None
        self.matching_keywords_list = []
        self.title_poses = False
        self.text_poses = False
        self.unique_type = self.pub['status_text']
        self.url = self.pub['url']
        self.html = None
        
    async def run(self):
        
        self.agency = [
            agency for agency in self.agencys if str(self.pub['crawler_id']) == agency['id']]
        
        await self.get_keyword_list()
        
        await self.build_html()
        
    async def get_keyword_list(self):
        
        for keywords_list in self.keywords_lists:
            
            # поля, в которых есть соотретствие списку ключевых слов
            fields = [field for field in [field for field in self.search_fields if [
            word for word in keywords_list if word['pos'] in self.pub[field]]
            ] if len([word for word in keywords_list if word['crc32'] in self.pub[field][
            word['pos']]]) == len(keywords_list)]
            
            if fields:
                
                self.matching_keywords_list.append(keywords_list)
                
                [setattr(self, field, True) for field in fields]
                
    async def build_html(self):
        
        self.html = render_to_string('pub.html', self.__dict__)