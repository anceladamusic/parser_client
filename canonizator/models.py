import mptt

from uuid import uuid1
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager

from django.db import models
from django.utils import timezone
from django_mysql.models import JSONField, Model
from django.core.validators import MinValueValidator, MaxValueValidator

##############################
# нормализованные публикации
class NormalizePublication(Model):

	class Meta():
		ordering = ['-date']
		verbose_name = 'нормализованная публикация'
		verbose_name_plural = 'нормализованные публикации'
		db_table = 'mainapp_normalizepublication'

	unique = 'уникальная'
	copy = 'скопированная'
	reprint = 'перепечатанная'
	incorrect = 'некорректная'

	STATUS_CHOICES = (
		(unique, 'уникальная'),
		(copy, 'скопированная'),
		(reprint, 'перепечатанная'),
		(incorrect, 'некорректная'),
	)

	id = models.UUIDField('идентификатор', default=uuid1, primary_key=True)
	title = models.CharField('заголовок', max_length=1024, blank=True, null=True)
	text = models.TextField('текст')
	date = models.DateTimeField('дата публикации', db_index=True)
	author = models.CharField('автор', max_length=512, blank=True, null=True)
	url = models.URLField('ссылка')
	crawler_id = models.CharField('идентификатор сборщика', max_length=40, null=True, db_index=True)
	crawl_date = models.DateTimeField('дата произведенной записи в БД', default=timezone.now)
	
	normalized_title = models.CharField('нормализованный заголовок', max_length=512, blank=True, null=True)
	normalized_text = models.TextField('нормализованный текст', blank=True, null=True)

	title_words = JSONField('слова заголовка')
	text_words = JSONField('слова текста')

	title_poses = JSONField('слова заголовка по частям речи')
	text_poses = JSONField('слова текста по частям речи')

	title_hashes = JSONField('хэши заголовка')
	text_hashes = JSONField('хэши текста')

	copying_date = models.DateTimeField('дата обработки', default=timezone.now)

	status_title_index = models.FloatField('Индекс уникальности заголовка', validators=[
		MinValueValidator(0), MaxValueValidator(1)], default=0)
	status_text_index = models.FloatField('Индекс уникальности текста', validators=[
		MinValueValidator(0), MaxValueValidator(1)], default=0)

	title_connected_pubs = JSONField('схожие заголовки')
	text_connected_pubs = JSONField('схожие тексты')

	status_title = models.CharField(
		max_length=200, verbose_name='уникальность', default=None, null=True, choices=STATUS_CHOICES)
	status_text = models.CharField(
		max_length=200, verbose_name='уникальность', default=None, null=True, choices=STATUS_CHOICES)

	parent_title = models.BigIntegerField(
		'родительская публикация по заголовку', default=None, null=True, db_index=True)
	parent_text = models.BigIntegerField(
		'родительская публикация по тексту', default=None, null=True, db_index=True)


	def as_dict(self):

		return {

			'title': self.title,
			'text': self.text,
			'date': self.date,
			'author': self.author,
			'url': self.url,
			'crawler_id': self.crawler_id,
			'crawl_date': self.crawl_date,

			'normalized_title': self.normalized_title,
			'normalized_text': self.normalized_text,
			
			'title_words': self.title_words,
			'text_words': self.text_words,

			'title_poses': self.title_poses,
			'text_poses': self.text_poses,		

			'title_hashes': self.title_hashes,
			'text_hashes': self.text_hashes,

			'copying_date': self.copying_date,

			'status_title_index': self.status_title_index,
			'status_text_index': self.status_text_index,

			'title_connected_pubs': self.title_connected_pubs,
			'text_connected_pubs': self.text_connected_pubs,

			'status_title': self.status_title,
			'status_text': self.status_text,

			'parent_title': self.parent_title,
			'parent_text': self.parent_text,

		}