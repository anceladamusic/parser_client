from django.http import HttpResponse
from django.shortcuts import render
from .models import *


def canonizator(request):

	normalized_pubs = NormalizePublication.objects.using('canonizator').filter(
		status_title__isnull=False, status_text__isnull=False)[:10]

	for pub in normalized_pubs:
		print (pub.as_dict())
		print ('----------------------')

	return HttpResponse(normalized_pubs)