from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^canonizator/$', views.canonizator, name='canonizator'),
]