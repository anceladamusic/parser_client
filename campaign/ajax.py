import json
import uuid
import datetime
import ipdb

from urllib import parse

from django.apps import apps
from django.urls import reverse
from django.http import JsonResponse
from django.template.loader import render_to_string

from .forms import *
from diagram.forms import UsersPeriod

from mainapp.base import Base as MainappBase
from .models import Keyword, Campaign
from .models import KeywordForm, CampaignForm, KeywordRenameForm
from diagram.dynamic import Dynamic
from diagram.dynamic_period_parser import DynamicPeriodParser
from campaign.user_campaign import UserCampaign
from remote.active_users import ActiveUsers

class Ajax():

	def __init__(self):
		self.forms = {
			1: CampaignNameForm,
			2: KeyWordsForm,
		}

	def read_ajax(self, request):

		if request.method == 'POST':

			string = json.loads(request.body.decode('utf-8'))
			
			# отправить собственный период
			if string['method'] == 'submit_users_period':
				
				params = MainappBase().request_params_to_dict(string['params'])
				
				date_from = params['date_from']
				
				date_to = params['date_to']
				
				period = (date_to - date_from).days
				
				page_uuid = string['page_uuid']

				user_id = request.session['client_id']

				session_key = request.session.session_key
				
				dynamic_period_object = DynamicPeriodParser(**{
					'datetime_from': date_from,
					'datetime_to': date_to,
					'period_keyword': 'custom',
					'include_retro': True,
				})
				
				dynamic_period_object.parse()
				
				# обновляем диаграммы всех кампаний
				diagrams = UserCampaign().build_data(page_uuid, user_id, session_key, dynamic_period_object)

				return JsonResponse({
					'string': 'ok',	
				})
				
			# собственный период динамики
			if string['method'] == 'users_period':
				
				form = UsersPeriod()
				
				period_html = render_to_string('dynamic/users_period.html', {
					'form': form,
				})
				
				return JsonResponse({
					'period_html': period_html,
				})
			
			# изменить количество дней в динамике
			if string['method'] == 'change_period':

				page_uuid = string['page_uuid']

				user_id = request.session['client_id']

				session_key = request.session.session_key

				dynamic_period_keyword = string['diagram_props']['dynamic']['period_keyword']
				
				dynamic_period_object = DynamicPeriodParser(**{
					'datetime_to': MainappBase().datetime_localize(datetime.datetime.now()),
					'period_keyword': dynamic_period_keyword,
					'include_retro': True,
				})
				
				dynamic_period_object.parse()
				
				# обновляем диаграммы всех кампаний
				diagrams = UserCampaign().build_data(page_uuid, user_id, session_key, dynamic_period_object)

				return JsonResponse({
					'string': 'ok',	
				})

			# отметка, что может клиент получать данные от сервера
			if string['method'] == 'campaign_can_receive_data':

				page_uuid = string['page_uuid']

				page_type = string['page_type']

				# данные активных пользователей
				ActiveUsers(**{
					'page_type': 'campaign_tree',
					'page_uuid': page_uuid,
					'can_receive_data': True,
				}).update(request)

				return JsonResponse({
					'string': 'ok',	
				})

			# подтвердить удаление
			if string['method'] == 'submit_delete':

				page_uuid = string['page_uuid']

				user_id = request.session['client_id']

				session_key = request.session.session_key

				node_id = string['node_id']

				dynamic_period_keyword = string['diagram_props']['dynamic']['period_keyword']
				
				dynamic_period_object = DynamicPeriodParser(**{
					'datetime_to': MainappBase().datetime_localize(datetime.datetime.now()),
					'period_keyword': dynamic_period_keyword,
					'include_retro': True,
				})
				
				dynamic_period_object.parse()

				model = apps.get_model('campaign', string['model'])

				element = model.objects.get(id=node_id)

				element.delete()

				# обновляем диаграммы всех кампаний
				diagrams = UserCampaign().build_data(page_uuid, user_id, session_key, dynamic_period_object)

				return JsonResponse({
					'deleted_id': element.id,	
				})

			# показать форму удалить ключевое слово
			if string['method'] == 'show_delete_form':

				model = apps.get_model('campaign', string['model'])

				object_id = string['id']

				name = string['name']

				verbose_name = model._meta.verbose_name

				element = model.objects.get(id=object_id)

				delete_html = render_to_string('confirm_delete.html', {
					'model': string['model'],
					'object_id': object_id,
					'name': element.name,
					'verbose_name': verbose_name,
				})

				return JsonResponse({
					'delete_html': delete_html,
				})

			# отправить форму переименовать ключевое слово
			if string['method'] == 'submit_keyword_rename':

				page_uuid = string['page_uuid']

				keyword_id = string['keyword_id']

				user_id = request.session['client_id']

				dynamic_period_keyword = string['diagram_props']['dynamic']['period_keyword']
				
				dynamic_period_object = DynamicPeriodParser(**{
					'datetime_to': MainappBase().datetime_localize(datetime.datetime.now()),
					'period_keyword': dynamic_period_keyword,
					'include_retro': True,
				})
				
				dynamic_period_object.parse()

				session_key = request.session.session_key

				params = MainappBase().request_params_to_dict(string['params'])

				keyword = Keyword.objects.get(id=keyword_id)

				form = KeywordRenameForm(params, instance=keyword)

				if form.is_valid():

					is_valid = True

					renamed_keyword = form.save()

					# обновляем диаграммы всех кампаний
					diagrams = UserCampaign().build_data(page_uuid, user_id, session_key, dynamic_period_object)
				else:

					is_valid = False

					renamed_keyword = keyword

				form_html = render_to_string('forms/form_usual.html', {
					'form': form,
					'form_head': form.get_form_head(),
					'form_id': form.get_form_id(),
					'form_submit': form.get_form_submit(),
					'form_submit_id': keyword_id,
				})

				return JsonResponse({
					'is_valid': is_valid,
					'form_html': form_html,
					'renamed_keyword': renamed_keyword.as_dict(),
				})

			# показать форму переименовать ключевое слово
			if string['method'] == 'show_rename_keyword_form':

				keyword_id = string['keyword_id']

				keyword = Keyword.objects.get(id=keyword_id)

				form = KeywordRenameForm(instance=keyword)

				form_html = render_to_string('forms/form_usual.html', {
					'form': form,
					'form_head': form.get_form_head(),
					'form_id': form.get_form_id(),
					'form_submit': form.get_form_submit(),
					'form_submit_id': keyword_id,
				})

				return JsonResponse({
					'form_html': form_html,
				})

			# отправить форму переименовать кампанию
			if string['method'] == 'submit_rename_campaign':

				campaign_id = string['campaign_id']

				params = MainappBase().request_params_to_dict(string['params'])

				campaign = Campaign.objects.get(id=campaign_id)

				form = CampaignForm(params, instance=campaign)

				if form.is_valid():

					is_valid = True

					renamed_campaign = form.save()

				else:

					is_valid = False

					renamed_campaign = campaign

				form_html = render_to_string('forms/form_usual.html', {
					'form': form,
					'form_head': form.get_form_head(),
					'form_id': form.get_form_id(),
					'form_submit': form.get_form_submit(),
					'form_submit_id': campaign_id,
				})

				return JsonResponse({
					'is_valid': is_valid,
					'form_html': form_html,
					'renamed_campaign': renamed_campaign.as_dict(),
				})

			# показать форму изменить наименование кампании
			if string['method'] == 'show_rename_campaign_form':

				campaign_id = uuid.UUID(string['campaign_id'])

				campaign = Campaign.objects.get(id=campaign_id)

				form = CampaignForm(instance=campaign)

				form_html = render_to_string('forms/form_usual.html', {
					'form': form,
					'form_head': form.get_form_head(),
					'form_id': form.get_form_id(),
					'form_submit': form.get_form_submit(),
					'form_submit_id': campaign_id,
				})

				return JsonResponse({
					'form_html': form_html,
				})

			# добавить ключевое слово
			if string['method'] == 'submit_keyword':

				page_uuid = string['page_uuid']

				user_id = request.session['client_id']
				
				dynamic_period_keyword = string['diagram_props']['dynamic']['period_keyword']
				
				dynamic_period_object = DynamicPeriodParser(**{
					'datetime_to': MainappBase().datetime_localize(datetime.datetime.now()),
					'period_keyword': dynamic_period_keyword,
					'include_retro': True,
				})
				
				dynamic_period_object.parse()

				session_key = request.session.session_key

				params = MainappBase().request_params_to_dict(string['params'])

				form = KeywordForm(params)

				if form.is_valid():

					is_valid = True

					new_keyword_object = form.save()

					new_keyword = new_keyword_object.as_dict()

					# обновляем диаграммы всех кампаний
					diagrams = UserCampaign().build_data(page_uuid, user_id, session_key, dynamic_period_object)

				else:

					new_keyword = {}

					is_valid = False

				form_html = render_to_string('forms/form_usual.html', {
					'form': form,
					'form_head': form.get_form_head(),
					'form_id': form.get_form_id(),
					'form_submit': form.get_form_submit(),
				})

				return JsonResponse({
					'is_valid': is_valid,
					'form_html': form_html,
					'new_keyword': new_keyword,
				})			

			# показать форму добавить ключевое слово (словосочетание)
			if string['method'] == 'show_add_keyword_form':

				campaign_id = uuid.UUID(string['campaign_id'])

				form = KeywordForm({'Campaign':campaign_id})

				form_html = render_to_string('forms/form_usual.html', {
					'form': form,
					'form_head': form.get_form_head(),
					'form_id': form.get_form_id(),
					'form_submit': form.get_form_submit(),
				})

				return JsonResponse({
					'form_html': form_html,
				})
			# создание кампании, отправит форму
			if string['method'] == 'create_campaign':

				campaign_name = string['campaign_name']

				keywords = string['keywords']

				campaign = Campaign.objects.create(
					name=campaign_name, user_id=request.session['client_id'])

				for keyword in keywords:

					Keyword(name=keyword, Campaign=campaign).save()

				redirect_url = reverse('campaign:campaign', kwargs={})

				return JsonResponse({
					'redirect_url': redirect_url,
				})
			# создание кампании, ввести имя
			if string['method'] == 'campaign_name':

				params = MainappBase().request_params_to_dict(string['params'])

				form = self.forms[string['form_id']](params)

				if form.is_valid():

					is_valid = True

					campaign_name = params['name']

				else:

					campaign_name = ''

					is_valid = False

				form = render_to_string('forms/form.html', {
					'header': form.get_header(),
					'name': form.get_name(),
					'submit_text': form.submit_text(),
					'id': form.get_id(),
					'back': form.get_back(),
					'back_id': form.get_back_id(),	
					'form': form,
				})

				return JsonResponse({
					'is_valid': is_valid,
					'form': form,
					'campaign_name': campaign_name,
				})