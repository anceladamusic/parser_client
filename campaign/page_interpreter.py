from uuid import uuid1

from django.template.loader import render_to_string

from interface_top.topbar import OauthTopbar

from .forms import *
from .user_campaign import UserCampaign

class PageInterpreter():

	def __init__(self, **kwargs):
		self.page_uuid = kwargs['page_uuid']
		self.topbar_elements = [
			{
				'id': 1,
				'name': '(1) Наименование',
				'back': 0,
				'back_id': 0,
				'next_id': 2,
			},
			{
				'id': 2,
				'name': '(2) Ключевые слова',
				'back': 1,
				'back_id': 1,
				'next_id': 3,
			},
		]

	# список кампанний
	def campaign(self, oauth_user, request):

		content = []

		campaign_tree = UserCampaign().build_tree(request.session['client_id'])

		topbar = OauthTopbar(**{
			'request': request,
			'campaign_name': 'Все кампании',
			'notifications': [],
			'element_html': 'mainpage/topbar_element.html',
			'elements': [],
			'selected': {}, 
		}).build()

		panel_left = render_to_string('left/panel_left.html', {
			'campaign_tree': campaign_tree,
		})

		content = render_to_string('content/content.html')

		html = render_to_string('campaign.html', {
			'panel_left': panel_left,
			'content': content,
			'campaign_tree': campaign_tree,
		})

		return {
			'uuid': self.page_uuid,
			'html': html,
			'topbar': topbar,
			'meta': {
				'meta_title': 'Все кампании',
				'meta_description': 'Статистика по всем кампаниям.',
			}
		}

	# создать кампанию
	def create_campaign(self, oauth_user, request):

		topbar = OauthTopbar(**{
			'request': request,
			'campaign_name': 'Создание кампании',
			'notifications': [],
			'element_html': 'campaign/topbar_element.html',
			'elements': self.topbar_elements,
			'selected': self.topbar_elements[0],
		}).build()

		campaign_name_form = render_to_string('forms/form.html', {
			'header': CampaignNameForm().get_header(),
			'name': CampaignNameForm().get_name(),
			'form': CampaignNameForm(),
			'submit_text': CampaignNameForm().submit_text(),
			'id': CampaignNameForm().get_id(),
			'back': CampaignNameForm().get_back(),
			'back_id': CampaignNameForm().get_back_id(),
		})

		keywords_form = render_to_string('forms/form_textarea.html', {
			'header': KeyWordsForm().get_header(),
			'name': CampaignNameForm().get_name(),
			'form': KeyWordsForm(),
			'submit_text': KeyWordsForm().submit_text(),
			'id': KeyWordsForm().get_id(),
			'back': KeyWordsForm().get_back(),
			'back_id': KeyWordsForm().get_back_id(),
		})

		content = render_to_string('create_campaign.html', {
			'campaign_name_form': campaign_name_form,
			'keywords_form': keywords_form,
			'topbar_elements': self.topbar_elements,
		})

		return {
			'uuid': self.page_uuid,
			'content': content,
			'topbar': topbar,
			'meta': {
				'meta_title': 'Создать кампанию',
				'meta_description': 'Создать кампанию и связать с ней ключевые слова.',
			}
		}