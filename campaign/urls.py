from django.urls import path
from . import views

urlpatterns = [
	path('user/campaign/', views.campaign, name='campaign'),
	path('user/campaign/create/', views.create_campaign, name='create_campaign'),
	path('user/campaign/update/<int:campaign_id>/', views.create_campaign, \
	 name='create_campaign'),
]