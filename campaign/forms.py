from django import forms


class CampaignNameForm(forms.Form):

	name = forms.CharField(label='Наименование', required=True)

	def get_header(self):
		return 'Введите наименование для Вашей кампании'

	def submit_text(self):
		return 'далее'

	def get_name(self):
		return 'CampaignName'

	def get_id(self):
		return 1

	def get_back(self):
		return False

	def get_back_id(self):
		return False


class KeyWordsForm(forms.Form):

	def get_header(self):
		return 'Введите ключевые слова(словосочетания), которые Вы хотите привязать к кампании'

	def submit_text(self):
		return 'готово'

	def get_name(self):
		return 'Keywords'

	def get_id(self):
		return 2

	def get_back(self):
		return True

	def get_back_id(self):
		return 1