import ipdb
import threading
import datetime

from django.template.loader import render_to_string

from mainapp.base import Base as MainappBase
from .models import Campaign, Keyword
from diagram.dynamic import Dynamic
from remote.cache import get_cache_table
from diagram.query import Query
from diagram.dynamic_period_parser import DynamicPeriodParser

class UserCampaign():

	def __init__(self):
		self.finded = None
		self.dynamic_objects = []

	# построить дерево campaign
	def build_tree(self, client_id):

		campaigns = [campaign.as_dict() for campaign in Campaign.objects.filter(user_id=client_id)]

		if campaigns:

			tree = {
				'user': client_id,
				'id': 0,
				'name': 'Все кампании',
				'type': 'all_campaign',
				'children': campaigns,
				'html': render_to_string('left/all_campaign_line.html', {
					'id': 0,
					'type': 'all_campaign',
					'name': 'Все кампании',	
				}),
				'diagram': {
					'dynamic': {
						'html': render_to_string('dynamic/dynamic.html', {
							'id': 0,
						}),
					},
				},
			}

		else:

			tree = {}

		return tree

	def get_all_dynamic_objects(self, node):

		if 'data' in node:

			self.dynamic_objects += [node['data']]

		if 'children' in node:

			for child in node['children']:

				self.get_all_dynamic_objects(child)
				
	# построить дерево объектов динамики
	def __build_dynamic_data(self, page_uuid, user_id, session_key, dynamic_period_object, point_load):
		
		if not dynamic_period_object:
			
			dynamic_period_object = DynamicPeriodParser(**{
				'datetime_to': MainappBase().datetime_localize(datetime.datetime.now()),
				'period_keyword': 'last 7 days',
				'include_retro': True,
			})
					
			dynamic_period_object.parse()
		
		campaing_objects = Campaign.objects.filter(user_id=user_id)

		tree = self.create_or_update_all_campaign(page_uuid, user_id, session_key, \
			dynamic_period_object, point_load)

		tree['children'] = [
			campaign.as_dynamic_data(**{
				'page_uuid': page_uuid,
				'user_id': user_id,
				'session_key': session_key,
				'dynamic_period_object': dynamic_period_object,
				'point_load': point_load,
			}) for campaign in campaing_objects
		]
		
		# вытаскиваем все объекты динамики из дерева
		self.get_all_dynamic_objects(tree)
		
		return {
			'tree': tree,
			'dynamic_objects': self.dynamic_objects,
			'dynamic_period_object': dynamic_period_object,
		}
				
	# построить данные для элемента campaign/keyword
	def build_point_data(self, page_uuid, user_id, session_key, dynamic_period_object=None, \
		query_fields=['text_poses', 'title_poses'], returning_fields=['date'], unique_type=None, \
		dynamic_object_id=None, point_load=False):
		
		dynamic_data = self.__build_dynamic_data(
			page_uuid, user_id, session_key, dynamic_period_object, point_load)
		
		result = self.make_query(**{
			'dynamic_objects': dynamic_data['dynamic_objects'],
			'query_fields': query_fields,
			'returning_fields': returning_fields,
			'group_by': dynamic_data['dynamic_period_object'].step_type,
			'unique_type': unique_type,
			'dynamic_object_id': dynamic_object_id,
			'point_load': point_load,
			'dynamic_period_object': dynamic_data['dynamic_period_object'],
		})
		
		return result
	
	# построить данные для всех элементов campaign
	def build_data(self, page_uuid, user_id, session_key, dynamic_period_object=None, \
		query_fields=['text_poses', 'title_poses'], returning_fields=['date'], unique_type=None, \
		dynamic_object_id=None, point_load=False):
		
		# строим объекты динамики
		dynamic_data = self.__build_dynamic_data(
			page_uuid, user_id, session_key, dynamic_period_object, point_load)

		# выполнить запрос на построение диаграмм
		t = threading.Thread(target=self.make_query, kwargs={
			'dynamic_objects': dynamic_data['dynamic_objects'],
			'query_fields': query_fields,
			'returning_fields': returning_fields,
			'group_by': dynamic_data['dynamic_period_object'].step_type,
			'unique_type': unique_type,
			'dynamic_object_id': dynamic_object_id,
			'point_load': point_load,
			'dynamic_period_object': dynamic_data['dynamic_period_object'],
		})
		t.start()

		return dynamic_data['tree']

	# выполнить запрос на построение диаграмм кампании и отправить слушателям
	def make_query(self, dynamic_objects, query_fields, returning_fields, group_by, unique_type, \
		dynamic_object_id, point_load, dynamic_period_object):

		pubs_table = get_cache_table('pubs')

		return Query(**{
			'keys': pubs_table.keys(),
			'dynamic_objects': dynamic_objects,
			'query_fields': query_fields,
			'returning_fields': returning_fields,
			'group_by': group_by,
			'unique_type': unique_type,
			'dynamic_object_id': dynamic_object_id,
			'point_load': point_load,
			'dynamic_period_object': dynamic_period_object,
		}).run()
		
	# построить элемент - все кампании
	def create_or_update_all_campaign(self, page_uuid, user_id, session_key, dynamic_period_object, point_load):

		keyword_objects = Keyword.objects.filter(Campaign__user_id=user_id)

		return {
			'type': 'all_campaign',
			'data': Dynamic(**{
				'page_uuid': page_uuid,
				'user_id': user_id,
				'session_key': session_key,
				'id': 0,
				'type': 'all_campaign',
				'name': 'Все кампании',
				'keywords_lists': [keyword.as_dict()['keywords'] for keyword in keyword_objects],
				'dynamic_period_object': dynamic_period_object,
				'point_load': point_load,
			}),
		}

	# найти в дереве объектов по идентификатору
	def get_by_id(self, node, object_id):

		if node['id'] == object_id:

			self.finded = node

		else:

			if 'children' in node:

				for child in node['children']: 

					self.get_by_id(child, object_id)

		return self.finded