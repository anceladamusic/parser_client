from django.apps import apps
from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin
from .models import *

class KeywordInline(admin.TabularInline):
	model = Keyword
	extra = 0

class Inlines():

	def __init__(self):
		self.model = {
			'campaign.campaign': [
				KeywordInline,
			],
		}

app_models = apps.get_app_config('campaign').get_models()

for model in app_models:

	if model.get_type() == 'MPTTModel':

		class ModelAdmin(DjangoMpttAdmin):

			if str(model._meta) in Inlines().model:

				inlines = Inlines().model[str(model._meta)]
				
		admin.site.register(model, ModelAdmin)

	else:

		class ModelAdmin(admin.ModelAdmin):
			list_display = [field.name for field in model._meta.fields]

			if str(model._meta) in Inlines().model:

				inlines = Inlines().model[str(model._meta)]

		admin.site.register(model, ModelAdmin)