from uuid import uuid1

from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render
# from silk.profiling.profiler import silk_profile

from .user_campaign import UserCampaign
from oauth.models import User as OauthUser
from mainapp.base import Base as MainappBase
from .page_interpreter import PageInterpreter
from remote.active_users import ActiveUsers

# @silk_profile(name='campaign page')
def campaign(request):

	session = MainappBase().check_authorize(request)

	if not session['client_id']:

		return HttpResponseRedirect(reverse('mainpage:index'))

	oauth_user = get_object_or_404(OauthUser, id=session['client_id'])

	page_uuid = uuid1()

	# данные диаграмм
	tree = UserCampaign().build_data(**{
		'page_uuid': str(page_uuid),
		'user_id': session['client_id'],
		'session_key': request.session.session_key,
	})

	# если нет кампаний, перенаправляем на создение кампаний
	if not tree['children']:

		return HttpResponseRedirect(reverse('campaign:create_campaign'))

	# данные активных пользователей
	ActiveUsers(**{
		'page_type': 'campaign_tree',
		'page_uuid': str(page_uuid),
		'data': [],
		'can_receive_data': False,
	}).update(request)

	page = PageInterpreter(**{'page_uuid': page_uuid}).campaign(oauth_user, request)

	content = page['html']

	return render(request, 'main.html', {
		'page_uuid': page['uuid'],
		'page_type': 'campaign_tree',
		'topbar': page['topbar'],
		'content': content,
		'session': session,
		'meta': page['meta'],
		'user_id': session['client_id'],
		'session_key': request.session.session_key,
	})

def create_campaign(request):

	session = MainappBase().check_authorize(request)

	if not session['client_id']:

		return HttpResponseRedirect(reverse('mainpage:index'))

	oauth_user = get_object_or_404(OauthUser, id=session['client_id'])

	# обновляем active_users
	page_type = 'campaign_create'

	page_uuid = uuid1()

	ActiveUsers(**{
		'page_type': page_type,
		'page_uuid': page_uuid,
		'can_receive_data': False,
	}).update(request)

	page = PageInterpreter(**{'page_uuid': page_uuid}).create_campaign(oauth_user, request)

	content = page['content']

	return render(request, 'main.html', {
		'page_uuid': page['uuid'],
		'page_type': 'campaign_create',
		'topbar': page['topbar'],
		'content': content,
		'session': session,
		'meta': page['meta'],
		'user_id': session['client_id'],
		'session_key': request.session.session_key,
	})