from uuid import uuid1
from mptt.models import MPTTModel, TreeForeignKey

from django import forms
from django.forms import ModelForm
from django.db import models
from django_mysql.models import Model, JSONField
from django.utils import timezone
from django.template.loader import render_to_string

from oauth.models import User as OauthUser
from mainapp.word_morpher import WordMorpher
from mainapp.cleaner import Cleaner
from diagram.dynamic import Dynamic

class Campaign(models.Model):

	class Meta:
		verbose_name = 'кампания'
		verbose_name_plural = 'кампании'

	id = models.UUIDField(default=uuid1, editable=False, db_index=True, primary_key=True)
	name = models.CharField('наименование', max_length=100, null=True)
	date = models.DateTimeField('дата создания', default=timezone.now)
	user = models.ForeignKey(OauthUser, on_delete=models.CASCADE, verbose_name='пользователь', null=True)

	def __str__(self):
		return '{0}'.format(self.name, self.user)

	def as_dict(self):

		return {
			'id': str(self.id),
			'type': 'campaign',
			'name': 'Кампания: {0}'.format(self.name.capitalize()),
			'user': self.user.id,
			'children': [keyword.as_dict() for keyword in getattr(self, 'keyword_set').all()],
			'html': render_to_string('left/campaign_line.html', {
				'type': 'campaign',
				'id': str(self.id),
				'name': self.name,
			}),
			'diagram': {
				'dynamic': {
					'html': render_to_string('dynamic/dynamic.html', {
						'id': str(self.id),
					}),
				},
			},
		}

	# данные для диаграммы
	def as_dynamic_data(self, page_uuid, user_id, session_key, dynamic_period_object, point_load):

		keywords = [keyword for keyword in getattr(self, 'keyword_set').all()]

		return {
			'id': str(self.id),
			'type': 'campaign',
			'data': Dynamic(**{
				'page_uuid': page_uuid,
				'user_id': user_id,
				'session_key': session_key,
				'id': str(self.id),
				'type': 'campaign',
				'name': 'Кампания: {0}'.format(self.name.capitalize()),
				'keywords_lists': [keyword.as_dict()['keywords'] for keyword in keywords],
				'dynamic_period_object': dynamic_period_object,
				'point_load': point_load,
			}),
			'children': [keyword.as_dynamic_data(**{
				'page_uuid': page_uuid,
				'user_id': user_id,
				'session_key': session_key,
				'dynamic_period_object': dynamic_period_object,
				'point_load': point_load,
			}) for keyword in getattr(self, 'keyword_set').all()],
		}

	def get_type():
		return 'models.Model'

class CampaignForm(ModelForm):

	class Meta:
		model = Campaign
		fields = ['name']

	def get_form_head(self):
		return 'Переименовать кампанию'

	def get_form_id(self):
		return 'rename_campaign_form'

	def get_form_submit(self):
		return 'submit_campaign'

class Keyword(Model):

	class Meta:
		verbose_name = 'ключевое слово'
		verbose_name_plural = 'ключевые слова'

	id = models.UUIDField(default=uuid1, editable=False, db_index=True, primary_key=True)
	name = models.CharField('слово (сочетания слов)', max_length=400, null=True)
	keywords = JSONField('нормальная форма')
	Campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, null=True, verbose_name='кампания')

	def __str__(self):
		return '{0}'.format(self.name)

	def save(self, *args, **kwargs):

		self.name = Cleaner().start(self.name)

		self.keywords = [WordMorpher().word_to_normal(word) for word in self.name.split()]

		super(Keyword, self).save(*args, **kwargs)


	def as_dict(self):

		return {
			'id': str(self.id),
			'type': 'keyword',
			'name': 'Ключевое слово (словосочетание): {0}'.format(self.name.capitalize()),
			'keywords': self.keywords,
			'parent_id': str(self.Campaign.id),
			'user': self.Campaign.user.id,
			'html': render_to_string('left/keyword_line.html', {
				'id': self.id,
				'type': 'keyword',
				'name': self.name,
			}),
			'diagram': {
				'dynamic': {
					'html': render_to_string('dynamic/dynamic.html', {
						'id': str(self.id),
					}),
				},
			},
		}

	# данные для диаграммы
	def as_dynamic_data(self, page_uuid, user_id, session_key, dynamic_period_object, point_load):
		return {
			'id': str(self.id),
			'type': 'keyword',
			'data': Dynamic(**{
				'page_uuid': page_uuid,
				'user_id': user_id,
				'session_key': session_key,
				'id': str(self.id),
				'type': 'keyword',
				'name': 'Ключевое слово (словосочетание): {0}'.format(self.name.capitalize()),
				'keywords_lists': [self.as_dict()['keywords']],
				'dynamic_period_object': dynamic_period_object,
				'point_load': point_load,
			}),
		}

	def get_type():
		return 'models.Model'

class KeywordForm(ModelForm):

	class Meta:
		model = Keyword
		fields = ['name', 'Campaign']

	def get_form_head(self):
		return 'Добавить ключевое слово (словосочетание)'

	def get_form_id(self):
		return 'add_keyword_form'

	def get_form_submit(self):
		return 'submit_keyword'

class KeywordRenameForm(ModelForm):

	class Meta:
		model = Keyword
		fields = ['name', 'Campaign']

	def get_form_head(self):
		return 'Изменить ключевое слово (словосочетание)'

	def get_form_id(self):
		return 'rename_keyword_form'

	def get_form_submit(self):
		return 'submit_keyword_rename'