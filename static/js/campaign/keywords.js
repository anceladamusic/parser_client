keywords_module = {};

//блоки
keywords_module.form_wrapper = $('.create_campaign_form_wrapper');
keywords_module.keywords_wrapper = '.keywords_wrapper';
keywords_module.keyword_input = $('.keyword_input');
keywords_module.keyword_text = '.keyword_text';
keywords_module.keyword_close = '.keyword_close';
keywords_module.submit_keywords = '.submit_keywords';
//блок ключевого слова
keywords_module.keyword_item = $('.keyword_item');

//переменные
keywords_module.keywords = []

//функции
keywords_module.read_ajax = keyWordsReadAjax;

//***************
// события
//***************

//отправить форму
keywords_module.form_wrapper.delegate(keywords_module.submit_keywords, 'click', function(){

	if (keywords_module.keyword_input){
		createKeyword();		
	}

	keywords = [];

	$.each(keywords_module.keywords, function(i){
		keywords.push(keywords_module.keywords[i]['text']);
	});

	if (keywords.length){


		parameters = {};
		parameters['keywords'] = keywords;
		parameters['campaign_name'] = create_campaign_module.campaign_name
		parameters['method'] = 'create_campaign';

		console.log(parameters);
		console.log('------------------end create campaign parameters');

		makeAjax(parameters);
	}

});

//удалить ключевое слово при нажатии
keywords_module.form_wrapper.delegate(keywords_module.keyword_close, 'click', function(){

	keyword_id = $(this).attr('data-id');

	keyword = false;

	$.each(keywords_module.keywords, function(i){
		if (keywords_module.keywords[i]['id'] == keyword_id){
			keyword = keywords_module.keywords[i];
			return false;
		}
	});


	keyword.html.remove();

	keywords_module.keywords.splice($.inArray(keyword, keywords_module.keywords), 1);

});

//при keywords wrapper выделить input
keywords_module.form_wrapper.delegate(keywords_module.keywords_wrapper, 'click', function(){

	keywords_module.keyword_input.focus();

});

//при нажати на enter
keywords_module.form_wrapper.delegate(keywords_module.keywords_wrapper, 'keydown', function(e) {

	var keyCode = e.keyCode || e.which; 

    if(keyCode == 13){

    	createKeyword();

    } else if (keyCode === 9){

    	e.preventDefault();

    	createKeyword();
    }

});



//***************
// функции
//***************

function keyWordsReadAjax(data, parameters){
	if (parameters['method'] == 'create_campaign'){
		window.location = data['redirect_url'];
	}
}

var createKeyword = function(){

	text = 	keywords_module.keyword_input.val();

	if (text){

		block = keywords_module.keyword_item.clone();

		close = block.children(keywords_module.keyword_close);

		close.attr('data-id', keywords_module.keywords.length+1);

		text_wrapper = block.children(keywords_module.keyword_text);

		text_wrapper.html(text)

		block.insertBefore($('.keyword_input_wrapper'));

		keywords_module.keyword_input.val('');

		keywords_module.keywords.push({
			'id': keywords_module.keywords.length+1,
			'text': text,
			'html': block,
			'html_close': close
		})

	}

}