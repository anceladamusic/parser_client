if (typeof campaign_tree != 'undefined'){

	campaign_dynamic_module = {};

	//blocks
	campaign_dynamic_module.dynamic_wrapper = $('.dynamic_wrapper');
	campaign_dynamic_module.page_uuid = page_uuid;
	campaign_dynamic_module.diagram_choose_line = '.diagram_choose_line';
	campaign_dynamic_module.dynamic_period_select = '#dynamic_period_select';
	campaign_dynamic_module.users_period = '#users_period';
	campaign_dynamic_module.users_period_confirm = '#users_period_confirm';
	campaign_dynamic_module.table = $('#pubs_list');

	//vars
	campaign_dynamic_module.period_keyword = 'last 7 days';
	campaign_dynamic_module.series_types = {
		'all': {
			'series': false,
			'categories': false,
			'count': false,
			'legend': false,
			'html': false,
			'count_html': false
		},
		'unique': {
			'series': false,
			'categories': false,
			'count': false,
			'legend': false,
			'html': false,
			'count_html': false
		},
		'copy': {
			'series': false,
			'categories': false,
			'count': false,
			'legend': false,
			'html': false,
			'count_html': false
		},
		'reprint': {
			'series': false,
			'categories': false,
			'count': false,
			'legend': false,
			'html': false,
			'count_html': false
		},
		/*'incorrect': {
			'series': false,
			'categories': false,
			'count': false,
			'legend': false,
			'html': false,
			'count_html': false
		}*/
	};
	campaign_dynamic_module.dynamic_diagram_property = {
	    chart: {
	        zoomType: 'xy'
	    },
	    title: false,
	    /*title: {
	        text: false
	    },*/
	    /*subtitle: {
	        text: false
	    },*/
	    xAxis: [{
        	categories: [],
        	crosshair: true
        }],
	    yAxis: [{ // Primary yAxis
	        labels: {
	            format: '{value} шт.',
	            style: {
	                color: Highcharts.getOptions().colors[2]
	            }
	        },
	        title: {
	            text: 'Количество публикаций',
	            style: {
	                color: Highcharts.getOptions().colors[2]
	            }
	        },
	        opposite: true

	    }],
	    tooltip: {
	        shared: true
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'left',
	        verticalAlign: 'top',
	        floating: true,
	        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
	    },
		plotOptions: {
			series: {
				point: {
					events: {
						click: function(){
							campaign_dynamic_module.point_click(this);
						}
					}
				}
			}
		},
	    series: [{
	        name: 'Серия 1',
	        color: '#687677',
	        /*type: 'spline',*/
	        yAxis: 0,
	        data: [],
	        marker: {
	            enabled: false
	        },
	        dashStyle: 'shortdot',
	        tooltip: {
	            valueSuffix: ' шт.',
	            valuePrefix: '{value.x}'
	        }

	    }, {
	        name: 'Серия 2',
	        color: '#5AB5DD',
	        /*type: 'spline',*/
	        yAxis: 0,
	        data: [],
	        marker: {
	            enabled: true
	        },
	        tooltip: {
	            valueSuffix: ' шт.'
	        }
	    }]
	};
	//functions
	campaign_dynamic_module.read_message = readDynamicMessage;
	campaign_dynamic_module.dynamic_init = dynamicInit;
	campaign_dynamic_module.dynamic_delete = dynamicDelete;
	campaign_dynamic_module.dynamic_select = dynamicSelect;
	campaign_dynamic_module.all_dynamics_loading = allDynamicsLoading;
	campaign_dynamic_module.all_dynamics_option_update = allDynamicsOptionUpdate;
	campaign_dynamic_module.read_ajax = dynamicReadAjax;
	campaign_dynamic_module.point_click = dynamicPointClick;

	//***********************
	// events
	//***********************
	
	//change period parameter
	campaign_dynamic_module.dynamic_wrapper.delegate(campaign_dynamic_module.dynamic_period_select, 'change', function(){
		
		loading_module.show();
		
		campaign_dynamic_module.period_keyword = $(this).find('option:selected').val();
		
		//if custom user period
		if (campaign_dynamic_module.period_keyword == 'custom'){
			
			parameters = {};
			parameters.method = 'users_period';
			
			campaign_dynamic_module.all_dynamics_loading(campaign_tree);
			
			makeAjax(parameters);
			
		//if others
		} else {
			
			parameters = {};
			parameters.page_uuid = campaign_dynamic_module.page_uuid;
			parameters.method = 'change_period';
	
			parameters.diagram_props = {
				'dynamic': {
					'period_keyword': campaign_dynamic_module.period_keyword
				}
			};
	
			//campaign_dynamic_module.all_dynamics_loading(campaign_tree);
	
			makeAjax(parameters);
			
		}
	});
	
	//submit users period
	modal_module.text.delegate(campaign_dynamic_module.users_period, 'keypress', function(e){

		if (e.which == 13){

			e.preventDefault();
			
			parameters = {};
			parameters.params = $(campaign_dynamic_module.users_period).serialize();
			parameters.page_uuid = campaign_dynamic_module.page_uuid;
			parameters.method = 'submit_users_period';
			makeAjax(parameters);

		}

	});
	
	modal_module.text.delegate(campaign_dynamic_module.users_period_confirm, 'click', function(){
		
		parameters = {};
		parameters.params = $(campaign_dynamic_module.users_period).serialize();
		parameters.page_uuid = campaign_dynamic_module.page_uuid;
		parameters.method = 'submit_users_period';
		makeAjax(parameters);
		
	});


	//change type: all, unique, reprint, copy
	campaign_dynamic_module.dynamic_wrapper.delegate(campaign_dynamic_module.diagram_choose_line, 'click', function(){

		node_id = $(this).attr('data-id');
		series_type = $(this).attr('data-type');

		campaign_dynamic_module.dynamic_select(node_id, series_type);

	});

	//***********************
	// functions
	//***********************
	
	//dynamic point on click
	function dynamicPointClick(point){
		
		console.log(point);
		console.log('-------------end of point');
		
		parameters = {};
		parameters.method = 'get_list_of_pubs';
		parameters.page_uuid = campaign_dynamic_module.page_uuid;
		parameters.range = point.options.range;
		parameters.range_type = point.options.type;
		parameters.object_id = point.options.object_id;
		parameters.object_type = point.options.object_type;
		parameters.unique_type = point.options.unique_type;
		
		loading_module.show();
		
		makeAjax(parameters);
	}
	
	//read ajax
	function dynamicReadAjax(data, parameters){
		
		if (parameters.method == 'users_period'){
			modal_module.put(data.period_html);
			modal_module.show();
		} else if (parameters.method == 'submit_users_period'){
			modal_module.hide();
		}
		
	}
	
	function readDynamicMessage(data){

		//if necessary page
		if (data.package.page_uuid == campaign_dynamic_module.page_uuid){

			//fill dynamic diagram
			if (data.method == 'dynamic_diagram_package'){
				
				loading_module.hide();

				//update option in other diagram
				campaign_dynamic_module.all_dynamics_option_update(
					campaign_tree, campaign_dynamic_module.period_keyword);

				node_id = data.package.id;

				series = data.package.series;

				count = data.package.count;

				categories = data.package.categories;

				type = data.package.type;

				legend = data.package.legend;

				title = data.package.title;

				subtitle = data.package.subtitle;

				nodes = [];
				getNodeInCampaignTreeById(campaign_tree, node_id, nodes);
				node = nodes[0];

				series_dynamic = node.diagram.dynamic.series;

				dynamic = node.diagram.dynamic;

				diagram = node.diagram.dynamic.diagram;

				//hide loading page
				diagram.hideLoading();

				dynamic.title_html.html(title);

				dynamic.subtitle_html.html(subtitle);

				$.each(series_dynamic, function(key, value){
					value.series = $.merge(series[0][key], series[1][key]);
					value.count = count[1][key];
					value.count_html.text(count[1][key]);
					value.legend = legend;
					value.categories = categories;
				});

				campaign_dynamic_module.dynamic_select(node_id, 'all');
			
			// make list of pubs in modal
			} if (data.method == 'pubs_list'){
				
				loading_module.hide();
				
				modal_module.show();
				
				modal_module.put(data.package.table);
				
				
			}

		}
		
	}

	//choose dynamic by id ant type
	function dynamicSelect(node_id, series_type){

		nodes = [];
		getNodeInCampaignTreeById(campaign_tree, node_id, nodes);
		node = nodes[0];

		diagram = node.diagram.dynamic.diagram;
		dynamic = node.diagram.dynamic;

		//check series
		$.each(dynamic.series, function(key, value){
			if (key == series_type){
				value.html.addClass('active');
			} else {
				value.html.removeClass('active');
			}
		});

		categories = dynamic.series[series_type].categories;
		series = dynamic.series[series_type].series;
		legend = dynamic.series[series_type].legend;

		diagram.xAxis[0].update({
			'categories': categories
		});

		diagram.series[0].update({
			'data': series.slice(0, categories.length),
			'id': node.id,
			'name': legend[0]
		});
		
		//add addition options to points
		$.each(diagram.series[0].points, function(i){
			
			point = diagram.series[0].points[i];
			data = series.slice(0, categories.length)[i][2];
			
			$.each(data, function(key, value){
				point.options[key] = value;
			});
			
		});
		
		diagram.series[1].update({
			'data': series.slice(-categories.length),
			'name': legend[1],
			'xAxes': 1
		});
		
		//add addition options to points
		$.each(diagram.series[1].points, function(i){
			
			point = diagram.series[1].points[i];
			data = series.slice(-categories.length)[i][2];
			
			$.each(data, function(key, value){
				point.options[key] = value;
			});
			
		});
		
		diagram.tooltip.update({
			'formatter': tooltipFormatter
		});

	}

	//get branch compaign tree by id
	function getNodeInCampaignTreeById(node, item_id, result){
		if (!node.length){
			if (node.id == item_id){
				result.push(node);
			}
		}
		if ('children' in node){
			$.each(node.children, function(i){
				getNodeInCampaignTreeById(node.children[i], item_id, result);
			});
		}
	}

	//initialization dynamic
	function dynamicInit(node){

		if (!node.length){

			dynamic = node.diagram.dynamic;

			campaign_dynamic_module.dynamic_wrapper.append(dynamic.html);

			dynamic.html = $('.dynamic_diagram_wrapper[data-id="'+ node.id +'"]');

			dynamic.title_html = dynamic.html.find('.dynamic_title');

			dynamic.subtitle_html = dynamic.html.find('.dynamic_subtitle');

			dynamic.period_html = dynamic.html.find('#dynamic_period_select');

			dynamic.series = $.extend(true, {}, campaign_dynamic_module.series_types);

			$.each(dynamic.series, function(key){
				html = dynamic.html.find(
					'.diagram_choose_line[data-type="'+ key+'"]');

				dynamic.series[key].html = html;

				dynamic.series[key].count_html = html.find('.diagram_choose_line_count');
			});

			//link diagram props
			dynamic.diagram = Highcharts.chart('dynamic'+node.id, 
				campaign_dynamic_module.dynamic_diagram_property);

			dynamic.diagram.showLoading();

		}

		if ('children' in node){
			$.each(node.children, function(i){
				dynamicInit(node.children[i]);
			});
		}

	}

	//update period option dynamic diagram
	function allDynamicsOptionUpdate(node, value){
		if (!node.length){

			period = node.diagram.dynamic.period_html;
			period.find('option[value="'+value+'"]').prop('selected', true);
		}
		if ('children' in node){
			$.each(node.children, function(i){
				allDynamicsOptionUpdate(node.children[i], value);
			});
		}
	}

	//all dynamic diagram statuses in loading
	function allDynamicsLoading(node){
		if (!node.length){

			node.diagram.dynamic.diagram.showLoading();

		}
		if ('children' in node){
			$.each(node.children, function(i){
				allDynamicsLoading(node.children[i]);
			});
		}
	}

	//delete diagram dynamic
	function dynamicDelete(node){
		if (!node.length){

			node.diagram.dynamic.html.remove();

		}
		if ('children' in node){
			$.each(node.children, function(i){
				dynamicDelete(node.children[i]);
			});
		}	
	}

	function tooltipFormatter(){

		if (this.points.length == 2){
			point_1 = {
				'dot': '<span style="color:'+ this.points[1].color+ '">\u25CF</span>',
				'value': this.points[1].y + ' шт.',
				'date': this.points[1].key
			};

			point_2 = {
				'dot': '<span style="color:' + this.points[0].color + '">\u25CF</span>',
				'value': this.points[0].y + ' шт.',
				'date': this.points[0].key
			};

			difference = this.points[1].y - this.points[0].y;

			if (difference < 0){

				point_different = '<span style="color:#C9413E">' +
				 String.fromCharCode(8595) + difference + ' ('+ parseInt(
				 	(difference/this.points[0].y)*100) +' %)' +
					' </span>';

			} else {

				point_different = '<span style="color:#1DCA93;">' + 
				String.fromCharCode(8593) + difference + ' ('+ parseInt(
					(difference/this.points[0].y)*100) +' %)' +
					' </span>';
			}

			line_1 = point_1.dot + ' ' + point_1.value + ' ' + point_1.date;
			line_2 = point_2.dot + ' ' + point_2.value + ' ' + point_2.date;

			return line_1 + '<br>' + '  против<br>' +line_2 + '<br>' + point_different;
		
		} else if (this.points.length == 1){
			point_1 = {
				'dot': '<span style="color:' + this.points[0].color + '">\u25CF</span>',
				'value': this.points[0].y + ' шт.',
				'date': this.points[0].key
			};
			
			return point_1.dot + ' ' + point_1.value + ' ' + point_1.date;
		}

		
	}

	campaign_dynamic_module.dynamic_init(campaign_tree);

	console.log(campaign_tree);
	console.log('-----------end campaign tree');
}