if (typeof campaign_tree != 'undefined'){

	campaign_left_module = {};
	//формы
	campaign_left_module.rename_campaign_form = '#rename_campaign_form';
	campaign_left_module.add_keyword_form = '#add_keyword_form';
	campaign_left_module.rename_keyword_form = '#rename_keyword_form';
	campaign_left_module.confirm_delete_wrapper = '#confirm_delete_wrapper';
	//блоки
	campaign_left_module.topbar_name = $('.topbar_name');
	campaign_left_module.left_panel_massive = $('#left_panel_massive');
	campaign_left_module.campaign_item = '.campaign_item';
	campaign_left_module.submit_keyword = '.submit_keyword';
	campaign_left_module.submit_campaign = '.submit_campaign';
	campaign_left_module.submit_keyword_rename = '.submit_keyword_rename';
	campaign_left_module.hide_manipulation_line = '.hide_manipulation_line';
	campaign_left_module.show_manipulation_line = '.show_manipulation_line';
	campaign_left_module.show_delete_form = '.show_delete_form';
	campaign_left_module.abort_delete = '.abort_delete';
	campaign_left_module.confirm_delete = '.confirm_delete';
	campaign_left_module.add_keyword_button = '.add_keyword';
	campaign_left_module.rename = '.rename';
	campaign_left_module.keyword_rename = '.keyword_rename';
	campaign_left_module.manipulation_line = '.manipulation_line';
	campaign_left_module.left_panel_massive = $('#left_panel_massive');
	//переменные
	campaign_left_module.selected_campaign = selected_campaign;

	//функции
	campaign_left_module.read_ajax = campaignLeftReadAjax;
	campaign_left_module.get_by_id = getById;
	campaign_left_module.delete_node = deleteNode;
	campaign_left_module.add_keyword = addKeyword;
	campaign_left_module.rename_keyword = renameKeyword;

	//*********************
	// события
	//*********************

	//подтвердить удаление объекта
	modal_module.text.delegate(campaign_left_module.confirm_delete, 'click', function(){

		node_id = $(this).attr('data-id');
		model = $(this).attr('data-model');

		campaign_left_module.delete_node(node_id, model);

	});

	modal_module.text.delegate(campaign_left_module.confirm_delete_wrapper, 'keypress', function(e){

		if (e.which == 13){

			e.preventDefault();

			node_id = $(campaign_left_module.confirm_delete).attr('data-id');
			model = $(campaign_left_module.confirm_delete).attr('data-model');

			campaign_left_module.delete_node(node_id, model);

		}

	});

	//показать форму удалить объект
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.show_delete_form, 'click', function(){

		parameters = {};
		parameters['id'] = $(this).attr('data-id');
		parameters['model'] = $(this).attr('data-model');
		parameters['name'] = $(this).attr('data-name');
		parameters['method'] = 'show_delete_form';

		makeAjax(parameters);
	});

	//отменить удаление
	modal_module.text.delegate(campaign_left_module.abort_delete, 'click', function(){
		modal_module.hide();
	});

	//отправить форму переименовать ключевое слово
	modal_module.text.delegate(campaign_left_module.submit_keyword_rename, 'click', function(){

		keyword_id = $(this).attr('data-id');
		form_props = $(campaign_left_module.rename_keyword_form).serialize();

		campaign_left_module.rename_keyword(keyword_id, form_props);
	});

	modal_module.text.delegate(campaign_left_module.rename_keyword_form, 'keypress', function(e){
		if (e.which == 13){

			e.preventDefault();

			keyword_id = $(campaign_left_module.submit_keyword_rename).attr('data-id');
			form_props = $(campaign_left_module.rename_keyword_form).serialize();

			campaign_left_module.rename_keyword(keyword_id, form_props);

		}
	});

	//отправить форму переименовать кампанию
	modal_module.text.delegate(campaign_left_module.submit_campaign, 'click', function(){
		parameters = {};
		parameters['campaign_id'] = $(this).attr('data-id');
		parameters['params'] = $(campaign_left_module.rename_campaign_form).serialize();
		parameters['method'] = 'submit_rename_campaign';

		makeAjax(parameters);
	});

	modal_module.text.delegate(campaign_left_module.rename_campaign_form, 'keypress', function(e){
		if (e.which == 13){

			e.preventDefault();
			parameters = {};
			parameters['campaign_id'] = $(campaign_left_module.submit_campaign).attr('data-id');
			parameters['params'] = $(campaign_left_module.rename_campaign_form).serialize();
			parameters['method'] = 'submit_rename_campaign';

			makeAjax(parameters);	

		}
	});

	//показать модальное окно переименовать кампанию
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.rename, 'click', function(){
		parameters = {};
		parameters['campaign_id'] = $(this).attr('data-id');
		parameters['method'] = 'show_rename_campaign_form';

		makeAjax(parameters);
	});


	//кампания. отправить форму добавить ключевое слово
	modal_module.text.delegate(campaign_left_module.submit_keyword, 'click', function(){

		campaign_left_module.add_keyword();

	});

	modal_module.text.delegate(campaign_left_module.add_keyword_form, 'keypress', function(e){
		if (e.which == 13){

			e.preventDefault();

			campaign_left_module.add_keyword();
		}
	});

	//кампания. показать модальное окно добавить ключевое слово в кампанию
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.add_keyword_button, 'click', function(){

		parameters = {}
		parameters['campaign_id'] = $(this).attr('data-id');
		parameters['method'] = 'show_add_keyword_form';

		makeAjax(parameters);

	});

	//показать модальное окно переименовать ключевое слово
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.keyword_rename, 'click', function(){

		parameters = {};
		parameters['keyword_id'] = $(this).attr('data-id');
		parameters['method'] = 'show_rename_keyword_form';
		makeAjax(parameters);
	});


	//скрыть manipulation line
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.hide_manipulation_line, 'click', function(){

		item_id = $(this).attr('data-id');

		hideAllManipulationLine(campaign_tree);
	});


	//показать manipulation line
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.show_manipulation_line, 'click', function(e){

		item_id = $(this).attr('data-id');

		show_manipulation_line(item_id);

	});


	//показать ключевые слова
	campaign_left_module.left_panel_massive.delegate(campaign_left_module.campaign_item, 'click', function(){


		item_id =  $(this).attr('data-id');

		selectById(campaign_tree, item_id);

	})

	//*********************
	// функции
	//*********************


	function campaignLeftReadAjax(data, parameters){

		// показать форму добавить ключевое слово
		if (parameters['method'] == 'show_add_keyword_form'){

			modal_module.put(data['form_html']);
			modal_module.show();
		//добавляем ключевое слово
		} else if (parameters['method'] == 'submit_keyword'){
			if (data['is_valid']){

				modal_module.hide();

				new_keyword = data['new_keyword'];
				
				// добаляем html
				campaign_left_module.left_panel_massive.append(new_keyword['html']);

				// ищем родителя и добавляем
				campaign = [];

				getById(campaign_tree, new_keyword['parent_id'], campaign);

				campaign[0]['children'].push(new_keyword);

				//привязываем ссылки на диаграмму
				campaign_dynamic_module.dynamic_init(new_keyword);

				//привязываем другие блоки
				linkHtmls(campaign_tree);

				//выбираем
				selectById(campaign_tree, campaign[0]['id']);

			} else {

				modal_module.put(data['form_html']);

			}
		//форма наименование кампании
		} else if (parameters['method'] == 'show_rename_campaign_form'){

			modal_module.put(data['form_html']);
			modal_module.show();
		// отправить форму переименовать слово
		} else if (parameters['method'] == 'submit_keyword_rename'){

			if (data['is_valid']){

				modal_module.hide();

				renamed_keyword = data['renamed_keyword']

				campaign_left_module.left_panel_massive.prepend(renamed_keyword['html']);

				keyword = []

				getById(campaign_tree, renamed_keyword['id'], keyword);

				keyword[0]['manipulation_html'].remove();

				keyword[0]['html'].remove();

				keyword[0]['name'] = renamed_keyword['name'];

				linkHtmls(campaign_tree);

				selectById(campaign_tree, renamed_keyword['parent_id']);

			} else {
				modal_module.put(data['form_html']);
			}
		//отправить форму переименовать кампанию.
		} else if (parameters['method'] == 'submit_rename_campaign'){
			
			if (data['is_valid']){

				modal_module.hide();

				renamed_campaign = data['renamed_campaign'];

				campaign_left_module.left_panel_massive.append(renamed_campaign['html']);

				campaign = [];

				getById(campaign_tree, renamed_campaign['id'], campaign);

				campaign[0]['manipulation_html'].remove();

				campaign[0]['html'].remove();

				campaign[0]['name'] = renamed_campaign['name'];

				linkHtmls(campaign_tree);

				selectById(campaign_tree, campaign[0]['id']);

			} else {

				modal_module.put(data['form_html']);

			}
		//показать форму изменить ключевое слово
		} else if (parameters['method'] == 'show_rename_keyword_form'){
			modal_module.put(data['form_html']);
			modal_module.show();
		// показать форму удалить
		} else if (parameters['method'] == 'show_delete_form'){

			modal_module.put(data['delete_html']);
			modal_module.show();
		//подтвердить удаление 
		} else if (parameters['method'] == 'submit_delete'){

			modal_module.hide();

			element = []

			getById(campaign_tree, parameters['node_id'], element);

			//удалить дочерние элементы
			removeChildren(element[0]);

			element[0]['html'].remove();
			element[0]['manipulation_html'].remove();

			removeById(campaign_tree, parameters['node_id']);

		}
	}

	//переименовать ключевое слово
	function renameKeyword(keyword_id, form_props){

		parameters  = {};
		parameters['keyword_id'] = keyword_id;
		parameters['params'] = form_props;
		parameters['method'] = 'submit_keyword_rename';
		parameters['page_uuid'] = page_uuid;

		parameters['diagram_props'] = {
			'dynamic': {
				'period_keyword': campaign_dynamic_module.period_keyword
			}
		};

		campaign_dynamic_module.all_dynamics_loading(campaign_tree);

		makeAjax(parameters);
	}


	//добавить ветку
	function addKeyword(){

		parameters = {};
		parameters['params'] = $(campaign_left_module.add_keyword_form).serialize();
		parameters['method'] = 'submit_keyword';
		parameters['page_uuid'] = page_uuid;

		parameters['diagram_props'] = {
			'dynamic': {
				'period_keyword': campaign_dynamic_module.period_keyword
			}
		};

		campaign_dynamic_module.all_dynamics_loading(campaign_tree);

		makeAjax(parameters);
	}

	//удалить ветку
	function deleteNode(node_id, model){

		parameters = {};
		parameters['node_id'] = node_id;
		parameters['model'] = model;
		parameters['method'] = 'submit_delete';
		parameters['page_uuid'] = page_uuid;

		parameters['diagram_props'] = {
			'dynamic': {
				'period_keyword': campaign_dynamic_module.period_keyword
			}
		};

		campaign_dynamic_module.all_dynamics_loading(campaign_tree);

		makeAjax(parameters);

	}

	//показать manipulation line
	var show_manipulation_line = function(item_id){

		item = [];

		getById(campaign_tree, item_id, item);

		hideAllManipulationLine(campaign_tree);

		if (item[0].manipulation_html.is(':visible')){
			item[0].manipulation_html.fadeOut(200);
		} else {
			item[0].manipulation_html.fadeIn(200);	
		}
	}

	var campaignTreeInit = function(){

		linkHtmls(campaign_tree);

		$.each($(campaign_left_module.campaign_item), function(i){
			if ($(this).attr('data-type') == 'all_campaign'){

				item_id = $(this).attr('data-id');

				selectById(campaign_tree, item_id);

				return false;
			}
		});

	}

	function getById(node, item_id, result){
		if (!node.length){
			if (node['id'] == item_id){
				result.push(node);
			}
		}
		if ('children' in node){
			$.each(node['children'], function(i){
				getById(node['children'][i], item_id, result);
			});
		}
	}


	var selectById = function(node, item_id){
		if (!node.length){
			if (node['id'] == item_id){

				node['selected'] = 1;
				node['html'].addClass('selected');

				$.each(node['diagram'], function(key, value){
					node['diagram'][key]['html'].delay(100).fadeIn(100);
				});

				campaign_left_module.topbar_name.html(node['name']);
				showChildren(node);

			} else {

				node['selected'] = 0;
				node['html'].removeClass('selected');
				node['diagram']['dynamic']['html'].fadeOut(100);

			}
		}
		if ('children' in node){
			$.each(node['children'], function(i){
				selectById(node['children'][i], item_id);
			});
		}
	}

	var showChildren = function(node){
		if ('children' in node){
			hideAllKeywords();
			$.each(node['children'], function(i){
				node['children'][i]['html'].insertAfter(node['html']);
				node['children'][i]['html'].fadeIn(200);
			});

		}
	}

	var hideAllKeywords = function(){
		$.each($(campaign_left_module.campaign_item), function(i){
			if ($(this).attr('data-type')=='keyword'){
				$(this).hide(200);
			}
		});
	}

	var hideAllManipulationLine = function(node){
		if (!node.length){
			node.manipulation_html.fadeOut(200);
		}
		if ('children' in node){
			$.each(node['children'], function(i){
				hideAllManipulationLine(node['children'][i]);
			});
		}
	}


	var linkHtmls = function(node){
		if (!node.length){
			$.each($(campaign_left_module.campaign_item), function(i){
				if ($(this).attr('data-type') == node.type && 
					$(this).attr('data-id') == node.id){
					node.html = $(this);
					node.manipulation_html = $(this).find($(campaign_left_module.manipulation_line));
				}
			});	
		}
		if ('children' in node){
			$.each(node['children'], function(i){
				linkHtmls(node['children'][i]);
			});
		}
	}

	//удалить из дерева по id
	var removeById = function(node, id){
		if ('children' in node){
			$.each(node['children'], function(i){
				if (node['children'][i]['id'] == id){

					//удалить dynamic
					campaign_dynamic_module.dynamic_delete(node['children'][i]);

					node['children'].splice(i, 1);

					return false;
				} else {
					removeById(node['children'][i], id);	
				}
			});
		}
	}

	//удалить все дочерние элементы
	var removeChildren = function(node){

		if ('children' in node){

			indexes = [];

			$.each(node['children'], function(i){

				node['children'][i]['html'].remove();
				node['children'][i]['manipulation_html'].remove();

				indexes.push(i)

			});

			$.each(indexes, function(i){
				node['children'].splice(i, 1);
			});

			node.length=0
		}
	}

	campaignTreeInit()

}