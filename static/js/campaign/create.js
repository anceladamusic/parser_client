if (typeof topbar_elements  != 'undefined'){

	create_campaign_module = {};
	//блоки
	create_campaign_module.form_wrapper = $('.create_campaign_form_wrapper');
	create_campaign_module.form_submit = '.form_submit';
	create_campaign_module.go_back = '.go_back';
	create_campaign_module.name_input = '#id_name';

	//переменные
	create_campaign_module.campaign_name = false;


	//функции
	create_campaign_module.read_ajax = createCampaignReadAjax;



	//****************
	// события
	//****************

	//вернуться назад
	create_campaign_module.form_wrapper.delegate(create_campaign_module.go_back, 'click', function(){
		back_id = parseInt($(this).attr('data-id'));


		showForm(back_id);
	});

	//отправить форму если enter на input
	create_campaign_module.form_wrapper.delegate(create_campaign_module.name_input, 'keypress', function(e){
		if (e.which == 13){
			e.preventDefault();

			topbar_selected = getSelected();

			form_id = topbar_selected['id'];

			form = getForm(form_id).find('form');

			nameSubmit(form, form_id);
		}
	});


	//отправить форму
	create_campaign_module.form_wrapper.delegate(create_campaign_module.form_submit, 'click', function(){

		form_id = parseInt($(this).attr('data-id'));
		form = getForm(form_id).find('form');

		nameSubmit(form, form_id);

	});


	//****************
	// функции
	//****************

	//отправить форму с именем кампании
	function nameSubmit(form, form_id){
		parameters = {};
		parameters['params'] = form.serialize();
		parameters['form_id'] = form_id;
		parameters['method'] = 'campaign_name';

		makeAjax(parameters);
	}

	function createCampaignReadAjax(data, parameters){
		if (parameters['method'] ==  'campaign_name'){

			form = getForm(parameters['form_id']);

			form.html(data['form']);

			if (data['is_valid'] == true){

				showForm(getTopbarElement(parameters['form_id'])['next_id']);

				create_campaign_module.campaign_name = data['campaign_name'];

			} else {

			}
		}
	}

	//инициализация всех форм
	function createCampaignInit(){
		$.each(topbar_elements, function(i){

			data_id = $(this)[0]['id'];

			topbar_elements[i]['form_html'] = $('.create_campaign_form_wrapper[data-id="'+data_id+'"]');

			topbar_elements[i]['html'] = $('.topbar_element_wrapper[data-id="'+data_id+'"]');
		});

		showForm(1);
	}

	//получить html форму topbar элемента
	var getForm = function(form_id){

		result = false;

		$.each(topbar_elements, function(i){
			if (topbar_elements[i]['id'] == form_id){
				
				result = topbar_elements[i]['form_html'];
				return false;
			}
		});
		return result;
	}

	//получить topbar элемент по id
	var getTopbarElement = function(form_id){
		result = false;
		$.each(topbar_elements, function(i){
			if (topbar_elements[i]['id'] == form_id){
				result = topbar_elements[i];
				return false;
			}
		});

		return result;
	}


	//показать конкретную форму
	var showForm = function(form_id){
		$.each(topbar_elements, function(i){
			if (topbar_elements[i]['id'] == form_id){
				topbar_elements[i]['form_html'].show();
				topbar_elements[i]['html'].addClass('selected');
				topbar_elements[i]['selected'] = 1
			} else {
				topbar_elements[i]['form_html'].hide();
				topbar_elements[i]['html'].removeClass('selected');
				topbar_elements[i]['selected'] = 0;
			}
		});
	}

	var getSelected = function(){
		result = false;
		$.each(topbar_elements, function(i){
			if (topbar_elements[i]['selected']){
				result = topbar_elements[i];
			}
		});
		return result;
	}


	createCampaignInit();

	console.log(topbar_elements);
	console.log('----------end topbar_elements');
}