effects_module = {};

//блоки
effects_module.topbar = $('.topbar_wrapper');
effects_module.page_content = $('.page_content');
effects_module.loading= $('.modal_window_loading');
effects_module.confirm = $('.confirm_wrapper');

//класс не включать подтверждение ссылки
effects_module.no_link_confirm = 'no_link_confirm';

//переменные
effects_module.mobile = checkScreen();

//****************
//события
//****************
//функции, ждущая загрузки картинок
$('body').waitForImages(function() {

	console.log('All images have loaded.');

}, function(loaded, count, success) {

   /*console.log(
   	loaded + ' of ' + count + ' images has ' + (success ? 'loaded' : 'failed to load') +  '.');
   $(this).addClass('loaded');*/

});

//cкрываем, если вне сслылки
$(document).on('click', function(e){
	if (effects_module.mobile == true){
		if ($('a:hover').length == 0){
			effects_module.confirm.hide(200);
		}
	}
});

//показываем повторный выбор
$('a').click(function(e){
	if (effects_module.mobile == true){

		if ($(this).hasClass(effects_module.no_link_confirm ) == false){
			//перехватываем переход по ссылке
			e.preventDefault();
			effects_module.confirm.css({'top':e.pageY-100});
			effects_module.confirm.css({'left':e.pageX-100});
			effects_module.confirm.show(200);
			
			url = ($(this)[0]['href']);
			effects_module.confirm[0]['dataset']['id'] = url;
		}
	}
});

//повторить выбор
effects_module.confirm.on('click', function(){
	window.location = $(this).attr('data-id');
});

//*****************
// функции
//*****************

function checkScreen(){
	result = false
	if ($(window).width() < 1024){
		result = true;
	}
	return result;
}