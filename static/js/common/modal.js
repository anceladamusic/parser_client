modal_module = {};

//блоки
modal_module.modal_window = $('.modal_window');
modal_module.page_content = $('.page_content');
modal_module.modal_close = $('.modal_close_button');
modal_module.text = $('.modal_text');

//functions
modal_module.show = modalShow;
modal_module.hide = modalHide;
modal_module.put = putText;
modal_module.append = append;

//*********************
//**events
//*********************


modal_module.modal_close.on('click', function(){
	modalHide();
});

//close window if escape
$(document).keyup(function(e) {
     if (e.keyCode == 27) {
     	if (modal_module.modal_window.is(':visible')==true){
     		modal_module.hide();	
     	}
    }
});

//close window if click out of window
$(document).click(function(event){
	if(!$(event.target).closest('.modal_content_inner').length){
		if($('.modal_content_inner').is(':visible')){
			modal_module.hide();
		}
	}
});

//*********************
//**functions
//*********************
function modalShow(){
	$('body').addClass('modal-open');
	modal_module.modal_window.show(200);
	modal_module.modal_window.css('display', 'flex');
	
}

function modalHide(){
	$('body').removeClass('modal-open');
	modal_module.modal_window.hide(200);
}

function putText(data){
	modal_module.text.html(data);
}

function append(data){
	modal_module.text.append(data);
}