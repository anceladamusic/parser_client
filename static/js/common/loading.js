var loading_module = {};

//block
loading_module.loading_block = $('.loading_wrapper');

//functions
loading_module.show = showLoading;
loading_module.hide = hideLoading;

//***************
// events
//***************

//***************
// functions
//***************

function showLoading(){
    $('body').addClass('modal-open');
    loading_module.loading_block.css({'display': 'flex'});
    loading_module.loading_block.show(200);
    
}

function hideLoading(){
    
    $('body').removeClass('modal-open');
    loading_module.loading_block.hide(200);
    
}