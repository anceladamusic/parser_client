import pickle
import redis
import aioredis
import asyncio
import random

from datetime import datetime

class ReadRedis():

	def __init__(self):
		self.queue_items_length = 30
		self.keys = redis.StrictRedis('localhost').keys()

	def __get_items(self, no):

		if no + self.queue_items_length < len(self.keys):

			items = self.keys[no: no+self.queue_items_length]

		else:

			items = self.keys[no: len(self.keys)]

		return items

	async def produce(self, queue, loop):

		for no in range(0, len(self.keys), self.queue_items_length):

			items = self.__get_items(no)

			for item in items:

				conn = await aioredis.create_connection('redis://localhost', loop=loop)

				await queue.put(len(pickle.loads(await conn.execute('get', item)))) 

				conn.close()

		await queue.put(None)

	async def consume(self, queue):

		while True:

			item = await queue.get()

			if item is None:

				break

			# print (item)
			# print ('--------------end item')

			await asyncio.sleep(0.1)

	def run(self):

		loop = asyncio.get_event_loop()

		queue = asyncio.Queue(loop=loop)

		producer = self.produce(queue, loop)

		consumer = self.consume(queue)

		loop.run_until_complete(asyncio.gather(producer, consumer))

start = datetime.now()

ReadRedis().run()

print (datetime.now() - start)
print ('----------------end time')