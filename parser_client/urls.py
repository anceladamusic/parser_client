from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^silk/', include(('silk.urls', 'silk'), namespace='silk')),
    url(r'^', include(('mainapp.urls', 'mainapp'), namespace='mainapp')),
    url(r'^', include('oauth.urls')),
    url(r'^', include('remote.urls')),
    url(r'^', include(('campaign.urls', 'campaign'), namespace='campaign')),
    url(r'^', include(('mainpage.urls', 'mainpage'), namespace='mainpage')),
    url(r'^', include('canonizator.urls')),
    url(r'^', include('sitemap.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)