import json
import tornado
import tornadoredis
from sockjs.tornado import SockJSConnection
from importlib import  import_module
from collections import namedtuple

import django
from django.conf import settings
from django.db import connection

_engine = import_module(settings.SESSION_ENGINE).SessionStore


ORDERS_REDIS_HOST = getattr(settings, 'ORDERS_REDIS_HOST', 'localhost')
ORDERS_REDIS_PORT = getattr(settings, 'ORDERS_REDIS_PORT', 6379)
ORDERS_REDIS_PASSWORD = getattr(settings, 'ORDERS_REDIS_PASSWORD', None)
ORDERS_REDIS_DB = getattr(settings, 'ORDERS_REDIS_DB', None)

def get_session(session_key):
	return _engine(session_key)

def get_user(session):

	if not connection.connection is None:
		connection.connection.close()
		connection.connection = None

	n_tuple = namedtuple('property', ['session'])

	django_request = n_tuple(session)

	return django.contrib.auth.get_user(django_request)

class Connection(SockJSConnection):
	def __init__ (self, *args, **kwargs):
		super(Connection, self).__init__(*args, **kwargs)
		self.listen_redis()

	
	@tornado.gen.engine
	def listen_redis(self):
		self.redis_client = tornadoredis.Client(
			host = ORDERS_REDIS_HOST,
			port = ORDERS_REDIS_PORT,
			password = ORDERS_REDIS_PASSWORD,
			selected_db = ORDERS_REDIS_DB
			)
		self.redis_client.connect()

		## канал
		yield tornado.gen.Task(self.redis_client.subscribe, [
			'work_package',
		])

		self.redis_client.listen(self.on_redis_queue)

	def send(self, msg_type, message):
		
		return super(Connection, self).send({
			'type': msg_type,
			'data':message,	
		})

	def on_open(self, info):

		self.django_session = get_session(info.get_cookie('sessionid').value)
		self.user = get_user(self.django_session)

	def on_message(self):
		pass

	# раздача слушателям
	def on_redis_queue(self, message):

		if message.kind == 'message':

			message_body =  json.loads(message.body)

			if message.channel == 'work_package':

				self.on_package(message_body)

	#send to user messages
	def on_package(self, message):

		if self.django_session:

			if message['data']['session_key'] == self.django_session.session_key:

				self.send('package', message)

	def on_close(self):

		self.redis_client.unsubscribe([
			'work_package'
		])
		
		self.redis_client.disconnect()