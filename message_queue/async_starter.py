import sys
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.abspath(os.path.join(BASE_DIR, os.pardir))

path = os.path.join(BASE_DIR, 'parser_client')

os.system('{0}/venv/bin/python {0}/manage.py async_server'.format(path))