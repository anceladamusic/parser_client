from django.conf import settings
from django.apps import AppConfig


class MessageQueueConfig(AppConfig):

    name = 'message_queue'