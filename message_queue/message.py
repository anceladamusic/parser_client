import json
import redis

from django.conf import settings
from django.db import models

class Message():

	def __init__(self):
		self.service_queue = redis.StrictRedis(
			host=settings.ORDERS_REDIS_HOST,
			port=settings.ORDERS_REDIS_PORT,
			db=settings.ORDERS_REDIS_DB,
			password=settings.ORDERS_REDIS_PASSWORD,
		).publish

	# функция отправить сообщение
	def make_queue(self, session_key, data, method):

		self.service_queue(
			'work_package',
			json.dumps({
				'data': {
					'session_key': session_key,
					'package': data,
					'method': method,
				}
			})
		)