from django.db import models
from django.utils import timezone

class User(models.Model):

    class Meta:

        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    name = models.CharField('имя', max_length=512)
    oauth_type = models.CharField('тип авторизации', max_length=128)
    registration_date = models.DateTimeField('дата регистрации', auto_now_add=True)
    last_visit_date = models.DateTimeField('дата последнего визита', auto_now=True)
    picture = models.URLField('картинка', max_length=400, null=True)
    profile = models.URLField('профиль', max_length=400, null=True)
    email = models.EmailField('электронная почта', max_length=400, null=True)

    def __str__(self):
        return self.name

    def get_type():
        return 'models.Model'

    def as_dict(self):
        return {
            'name': self.name,
            'oauth_type': self.oauth_type,
            'picture': str(self.picture),
            'profile': str(self.profile),
            'email': self.email,
        }