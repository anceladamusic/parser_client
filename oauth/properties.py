class Properties():

	def __init__(self):
		self.ya_settings = {
		    'code_url' : 'https://oauth.yandex.ru/authorize?',
		    'id' : '44b84c2420b546c7b74894e7698c8ab6',
		    'secret' : 'ee0d9510bcdf4b2b8cb094d0831c1bb1',
		    'type' : 'code',
		    'display' : 'popup',
		    'state' : 'name.of.site',
		}

		self.vk_settings = {
		    'code_url' : 'https://oauth.vk.com/authorize?',
		    'token_url' : 'https://oauth.vk.com/access_token?',
		    'id' : '6407664',
		    'redirect' : 'http://localhost:22000/oauth/vk/',
		    'display' : 'popup',
		    'scope' : '',
		    'type' : 'code',
		    'api' : '5.80',
		    'secret' : 'tUXNiwlkadYS2LSzcybT',
		}


		self.fb_settings = {
		    'code_url' : 'https://www.facebook.com/v2.9/dialog/oauth?',
		    'token_url' : 'https://graph.facebook.com/v2.9/oauth/access_token?',
		    'id' : '226402031269584',
		    'redirect' : 'http://localhost:22000/oauth/fb/',
		    'state' : 'name.of.site',
		    'display' : 'popup',
		    'scope' : '',
		    'type' : 'code',
		    'secret' : '2025c9ecd0a69bf81c2029531e9606ef',
		}


		self.gp_settings = {
		    'code_url' : 'https://accounts.google.com/o/oauth2/auth?',
		    'id' : '855831152944-hh21jt3hrdv5het3tror9203bdn4ue78.apps.googleusercontent.com',
		    'redirect' : 'http://localhost:22000/oauth/gp/',
		    'display' : '',
		    'scope' : 'https://www.googleapis.com/auth/userinfo.profile',
		    'type' : 'code',
		    'secret' : '6cgpzv1xN-C-9IIRP8W-pj3Y',
		}


		

		self.auth_settings = {
			'vk': {
				'name' : 'VK',
				'url' : '{code_url}client_id={id}&display={display}&redirect_uri={redirect}&scope={scope}&response_type={type}&v={api}'.format(
					**self.vk_settings,
				)
			},
			'fb' : {
				'name' : 'FB',
				'url' : '{code_url}client_id={id}&display={display}&redirect_uri={redirect}&scope={scope}&response_type={type}'.format(
					**self.fb_settings,
				)
			},
			'gp' : {
				'name' : 'G+',
				'url' : '{code_url}redirect_uri={redirect}&response_type={type}&client_id={id}&scope={scope}'.format(
					**self.gp_settings,
				)
			},
			'ya' : {
				'name' : 'YA',
				'url' : 'https://oauth.yandex.ru/authorize?response_type={type}&client_id={id}&display={display}&force_confirm=yes&state={state}'.format(
					**self.ya_settings,
				)
			},
		}