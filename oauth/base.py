from .models import User

class Base():

	def __init__(self):

		pass

	def update_or_create_n_add_to_session(self, client_id, name, oauth_type, request):

		obj, created = User.objects.update_or_create(
			client_id=client_id,
			name=name,
			oauth_type=oauth_type,
		)
		
		request.session['authorized'] = True
		request.session['name'] = name
		request.session['client_id'] = obj.client_id