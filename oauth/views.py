import ipdb

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.core.mail import send_mail
from django.contrib import messages

from . import models
from .properties import Properties

import json
import requests

prop = Properties()

def vk(request):

    code = request.GET.get('code', False)

    if code:

        prop.vk_settings['code'] = code

        link = '{token_url}client_id={id}&client_secret={secret}&redirect_uri={redirect}&code={code}'.format(
            **prop.vk_settings)
        
        user_props = json.loads(requests.get(link).text)
        
        link = 'https://api.vk.com/method/{0}?user_id={1}&access_token={2}&v={3}'.format(
            'users.get',
            user_props['user_id'],
            user_props['access_token'],
            prop.vk_settings['api'],
        )

        response = json.loads(requests.get(link).text)

        photo_link = 'https://api.vk.com/method/users.get?access_token={}&user_ids={}&fields=photo_200&v={}'.format(
            user_props['access_token'],
            response['response'][0]['id'],
            prop.vk_settings['api'],
        )
        
        photo_props = json.loads(requests.get(photo_link).text)

        first_name = response['response'][0]['first_name']
        last_name = response['response'][0]['last_name']
        full_name = '{0} {1}'.format(last_name, first_name)

        user = models.User.objects.update_or_create(
            picture=photo_props['response'][0]['photo_200'],
            profile='https://vk.com/id{0}'.format(response['response'][0]['id']),
            name=full_name,
            oauth_type='VK'
        )

        if not 'authorized' in request.session:

            request.session['authorized'] = True
            request.session['name'] = full_name
            request.session['client_id'] = user[0].id

            return HttpResponseRedirect(reverse('mainpage:index'))

        else:

            return HttpResponseRedirect(reverse('mainpage:index'))
    else:
        return HttpResponse('Error')


def fb(request):

    code = request.GET.get('code', False)

    if code:

        prop.fb_settings['code'] = code

        link = '{token_url}client_id={id}&redirect_uri={redirect}&client_secret={secret}&code={code}'.format(
            **prop.fb_settings)
        
        data = json.loads(requests.get(link).text)

        link = 'https://graph.facebook.com/v2.9/me?access_token={0}'.format(data['access_token'])
        data = json.loads(requests.get(link).text)

        user = models.User.objects.update_or_create(
            picture='https://graph.facebook.com/{0}/picture?type=square'.format(data['id']),
            profile='https://facebook.com/app_scoped_user_id/{0}'.format(data['id']),
            name=data['name'],
            oauth_type='FB',
        )

        if not 'authorized' in request.session:

            request.session['authorized'] = True
            request.session['name'] = data['name']
            request.session['client_id'] = user[0].id

            return HttpResponseRedirect(reverse('mainpage:index'))

        else:

            return HttpResponseRedirect(reverse('mainpage:index'))

    else:
        return HttpResponse('Error')


def gp(request):
    code = request.GET.get('code', False)
    if code:
        link = 'https://accounts.google.com/o/oauth2/token'
        params = {
            'client_id' : prop.gp_settings['id'],
            'client_secret' : prop.gp_settings['secret'],
            'redirect_uri' : prop.gp_settings['redirect'],
            'grant_type' : 'authorization_code',
            'code' : code,
        }
        data = json.loads(requests.post(link, params).text)

        params['access_token'] = data['access_token']

        link = 'https://www.googleapis.com/oauth2/v3/userinfo'
        data = json.loads(requests.get(link, params).text)

        if 'profile' in data:

            page = data['profile']

        else:

            page = None

        user = models.User.objects.update_or_create(
            name=data['name'],
            oauth_type='G+',
            picture=data['picture'],
            profile=page,
        )
        if not 'authorized' in request.session:

            request.session['authorized'] = True
            request.session['name'] = data['name']
            request.session['client_id'] = user[0].id

            return HttpResponseRedirect(reverse('mainpage:index'))

        else:
            return HttpResponseRedirect(reverse('mainpage:index'))

    else:
        return HttpResponse('Error')


def ya(request):
    code = request.GET.get('code', False)
    if code:
        link = 'https://oauth.yandex.ru/token'
        params = {
            'client_id' : prop.ya_settings['id'],
            'client_secret' : prop.ya_settings['secret'],
            'grant_type' : 'authorization_code',
            'code' : code,
        }
        data = json.loads(requests.post(link, params).text)

        token = data['access_token']

        link = 'https://login.yandex.ru/info?format=json&with_openid_identity=true&oauth_token={0}'.format(token)
        data = json.loads(requests.get(link).text)

        user = models.User.objects.update_or_create(
            picture='https://avatars.yandex.net/get-yapic/{0}/islands-200'.format(
                data['default_avatar_id']),
            email=data['default_email'],
            name=data['real_name'],
            oauth_type='YA'
        )

        if not 'authorized' in request.session:

            request.session['authorized'] = True
            request.session['name'] = data['real_name']
            request.session['client_id'] = user[0].id

            return HttpResponseRedirect(reverse('mainpage:index'))
    else:
        return HttpResponse('Error')