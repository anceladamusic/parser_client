import json

from django.http import JsonResponse
from django.template.loader import render_to_string

from campaign.ajax import Ajax as CampaignAjax
from diagram.ajax import Ajax as DiagramAjax
from remote.ajax import Ajax as RemoteAjax
from remote.active_users import get_active_users, remove_from_active_users
from remote.agencys import get_agencys

class Ajax():

	def __init__(self):
		pass

	def make_ajax(self, request):

		campaign_result = CampaignAjax().read_ajax(request)

		if campaign_result:

			return campaign_result

		diagram_result = DiagramAjax().read_ajax(request)

		if diagram_result:

			return diagram_result

		remote_result = RemoteAjax().read_ajax(request)

		if remote_result:

			return remote_result

		# общие ajax запросы
		if request.method == 'POST':

			string = json.loads(request.body.decode('utf-8'))

			# информация о активных пользователях в консоль
			if string['method'] == 'get_active_users':

				return JsonResponse({
					'string': get_active_users(),
				})
			
			# информация о агентствах
			if string['method'] == 'get_agencys':
				
				return JsonResponse({
					'string': get_agencys(),
				})

			# удалить страницу из active_users
			if string['method'] == 'remove_page_from_active_users':

				if string['user_id']:

					user_id = int(string['user_id'])

					page_uuid = string['page_uuid']

					session_key = string['session_key']

					remove_from_active_users(user_id, page_uuid, session_key)

				return JsonResponse({
					'string': 'ok',	
				})