import binascii
import pymorphy2

class WordMorpher():

	def __init__(self):
		self.morph = pymorphy2.MorphAnalyzer()

	def __convert_crc32(self, value):

		value_bytes=bytes(value, 'utf-8')

		return binascii.crc32(value_bytes)


	def word_to_normal(self, word):

		parsed_to_morph = self.morph.parse(word)[0]

		morph_normal = self.morph.parse(parsed_to_morph.normal_form)[0]

		pos = morph_normal.tag.POS

		if not pos:

			pos = 'unkn'

		return {
			'normal_form': morph_normal.normal_form,
			'pos': pos,
			'crc32': self.__convert_crc32(morph_normal.normal_form)
		}