import math
import datetime
import pytz
import json
import threading

from urllib.parse import parse_qs
from dateutil.parser import parse

from django.apps import apps
from django.conf import settings
from django.utils import timezone


from .tree_elems import TreeElems
from oauth.properties import Properties


django_timezone = settings.TIME_ZONE
timezone.activate(pytz.timezone(django_timezone))
current_tz = timezone.get_current_timezone()

class Base():

	def __init__(self):
		self.mainapp_models = [model for model in apps.get_app_config('mainapp').get_models()]
		self.django_timezone = settings.TIME_ZONE

	# список интервалов времени
	def get_list_of_ranges(self, date_from, date_to, step, range_type):

		if range_type == 'hourrange':

			dates = [date for date  in self.hourrange(date_from, date_to)]

		elif range_type == 'daterange':

			dates = [date for date  in self.daterange(date_from, date_to)]

		if not dates:
			dates = [date_from, date_to]

		result = []

		for i in range(0, len(dates), step):

			if i+step < len(dates):

				result.append([
					dates[i],
					dates[i+step]-datetime.timedelta(seconds=1)
				])

			else:

				result.append([
					dates[i],
					date_to,
				])

		result[0][0] = date_from
		
		result[len(result)-1][1] = date_to

		return result

	def daterange(self, start_date, end_date):

		delta = end_date - start_date

		result = []

		for day in range(delta.days + 1):

			result.append(start_date + datetime.timedelta(days = day))
		
		return result

	def weekrange(self, start_date, end_date):

		delta = end_date - start_date

		result = []

		for week in range(0, delta.days + 1, 7):

			result.append(start_date + datetime.timedelta(days=week))

		return result

	def hourrange(self, start_date, end_date):

		for hour in range(int((end_date - start_date).total_seconds()//3600) + 1):

			yield start_date + datetime.timedelta(hours=hour)
			
	# получить первую дату года
	def get_year_first_day(self, date):
		
		year = date.year
		
		return self.datetime_localize(datetime.datetime(year=year, month=1, day=1, hour=0, minute=0, second=0))

	# получить квартал из даты
	def get_quarter(self, date):

		return math.ceil(date.month/3)

	# получить первую дату квартала
	def get_quarter_first_day(self, date):

		quarter = self.get_quarter(date)

		return self.datetime_localize(datetime.datetime(date.year, 3 * quarter - 2, 1))


	def datetime_localize(self, date_or_datetime):

		timezone.activate(pytz.timezone(self.django_timezone))

		current_tz = timezone.get_current_timezone()

		return current_tz.localize(date_or_datetime)


	def request_params_to_dict(self, params):
		result = {}

		request_params = parse_qs(params)

		for key, value in request_params.items():
			
			result[key] = self.__write_params_line(value)

		return result

	def __write_params_line(self, value):
		result = []
		
		for value_line in value:
			
			try:
				
				date = self.datetime_localize(parse(value_line, dayfirst=True))
				
				result.append(date)
				
			except:
			
				if value_line.isdigit():
					
					result.append(int(value_line))
					
				else:
					
					result.append(value_line)
				
		if len(result) == 1:
			
			return result[0]
		
		return result

	def build_meta(self, element):

		return {
			'meta_title': element['title'],
			'meta_description': element['short_description'],
		}


	def check_authorize(self, request):

		result = {}

		if 'authorized' in request.session:

			result['authorized'] = request.session['authorized']

			result['name'] = request.session['name']

			result['client_id'] = request.session['client_id']

		else:

			result['authorized'] = False

			result['name'] = False

			result['client_id'] = False

		return result