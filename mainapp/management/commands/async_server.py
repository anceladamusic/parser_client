import tornado
from sockjs.tornado import SockJSRouter

from django.core.management.base import BaseCommand

from message_queue.service import Connection

class Command(BaseCommand):
	def handle(self, **options):
		router = SockJSRouter(Connection, '/messages')
		app = tornado.web.Application(router.urls)
		app.listen(8989)
		tornado.ioloop.IOLoop.instance().start()