# класс выделяет слова в тексте, добавляя теги <b>
# принимает список, отдает список

import pymorphy2
from .cleaner import Cleaner
from .queryset import Queryset
class HtmlMarker():

	def __init__(self):
		self.morph = pymorphy2.MorphAnalyzer()

	def start(self, texts, query_words):

		query_words = Cleaner().start(query_words)

		query_words = [self.__to_normal_form(word) for word in query_words.split(' ')]

		result = []

		for text in texts:

			text = Cleaner().start(text).split(' ')

			result_line = []

			for word in text:

				word_morth = self.morph.parse(word)[0]

				if any(w == word_morth.normal_form for w in query_words):

					result_line.append('<b>{0}</b>'.format(word))

				else:

					result_line.append(word)

			result.append(' '.join(result_line))

		return result

	def __to_normal_form(self, word):

		return self.morph.parse(word)[0].normal_form