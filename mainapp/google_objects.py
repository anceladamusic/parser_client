class WebsiteSettings():
	def __init__(self):
		self.url= "http://news-mining.online"

#################
# Информация о сайте
#################
class WebsiteInfo():

	def __init__(self):
		self.type = 'WebSite'
		self.name = 'Онлайн мониторинг публикаций в политических информационных агентствах'
		self.alternateName = 'news-mining online'
		self.url = WebsiteSettings().url
		# self.image = '{0}/media/colored.png'.format(self.url)
		# self.sameAs = [
		# 	'https://vk.com/neiroprofi',
		# ]

###################
# Корпоративные контакты
###################

class Organization():

	def __init__(self):
		self.type = 'Organization'
		self.url = WebsiteSettings().url
		# self.logo = '{0}/media/colored.png'.format(self.url)
		self.contactPoint = [
			contactPoint().as_dict(),
		]

class contactPoint():
	def __init__(self):
		self.type = 'ContactPoint'
		self.telephone = '+7-927-679-3788'
		self.contactType = 'customer service'

	def as_dict(self):
		return {
			'type': self.type,
			'telephone': self.telephone,
			'contactType': self.contactType,
		}
#####################
# Услуги
#####################

class ServiceInfo():

	def __init__(self):
		self.type = 'Service'
		self.name = WebsiteInfo().name
		self.serviceType = 'Информационная сервисная служба'
		self.serviceArea = serviceArea().as_dict()
		self.availableChannel = availableChannel().as_dict()

class serviceArea():

	def __init__(self):
		self.type = 'AdministrativeArea'
		self.name = 'Россия'

	def as_dict(self):
		return {
			'type': self.type,
			'name': self.name,
		}


class availableChannel():

	def __init__(self):
		self.type = 'ServiceChannel',
		self.name = 'Пожелания и предложения'
		self.availableLanguage = availableLanguage().as_dict()
		self.serviceLocation = serviceLocation().as_dict()

	def as_dict(self):
		return {
			'type': self.type,
			'name': self.name,
			'availableLanguage': self.availableLanguage,
			'serviceLocation': self.serviceLocation,
		}

class availableLanguage():
	def __init__(self):
		self.type = 'Language'
		self.name = 'Russian'
		self.alternateName = 'ru'

	def as_dict(self):
		return {
			'type': self.type,
			'name': self.name,
			'alternateName': self.alternateName,
		}

class serviceLocation():

	def __init__(self):
		self.type = 'LocalBusiness'
		self.name = WebsiteInfo().name
		self.telephone = '+7-927-679-3788'
		self.image = '{0}/media/colored.png'.format(WebsiteSettings().url)
		self.address = address().as_dict()

	def as_dict(self):
		return {
			'type': self.type,
			'name': self.name,
			'address': self.address,
			'image': self.image,
			'telephone': self.telephone,
		}

class address():
	def __init__(self):
		self.type = 'PostalAddress'
		self.streetAddress = 'ул. Спартаковская, д. 2  корп 1, оф. 262'
		self.addressLocality = 'Казань'
		self.addressRegion = 'Россия'
		self.postalCode = '420049'

	def as_dict(self):
		return {
			'type': self.type,
			'streetAddress': self.streetAddress,
			'addressLocality': self.addressLocality,
			'addressRegion': self.addressRegion,
			'postalCode': self.postalCode,
		}



###################
# список с объектами
###################
class ListItem():
	def __init__(self):
		self.type = 'ListItem'
		self.position = None
		self.item = None

	def as_dict(self):
		return {
			'type': self.type,
			'position': self.position,
			'item': self.item,
		}

class Item():
	def __init__(self):
		self.id = None
		self.name = None

	def as_dict(self):
		return {
			'id': self.id,
			'name': self.name
		}