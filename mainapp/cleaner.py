import re
class Cleaner():

	def __init__(self):
		self.caret_symbols = [
			'\n',
			'\t',
			'\r',
			'\f',
			'\v',
			'\xa0',
			'\u200b',
			'\u2028',
			'\u2029',
		]
		self.any_tag = '<[^>]*>'
		self.any_html = '&[#a-zA-Z0-9]{2,10};'
		self.any_time = '[0-2][0-4]:[0-5][0-9]'
		self.any_year = '[0-2][0-9][0-9][0-9]'
		self.any_slashes = [
			'/',
			'\\\\',
			'\|',
		]
		self.symbols = [
			'[()]',
			'[[]]',
			'[{}]',
			'[«»]',
			'[„“]',
			'[“”]',
			'[‘’]',
			'["'']',
			'[―#%$@&*^+=_–—‒:;@№.,!?]',
			'[--]',
			'[\^]',
		]
		self.multi_spaces = '\s{2,}'
		self.some_cyrillic = {
			'Ё': 'Е',
			'ё': 'е',
		}
		self.funcs = [
			self.__remove_caret,
			self.__remove_tag,
			self.__remove_html,
			self.__remove_time,
			self.__remove_year,
			self.__remove_slash,
			self.__remove_other_symbols,
			self.__replace_some_cyrillic,
			self.__replace_multi_spaces,
		]

		self.serve_string = None


	def __remove_caret(self):

		for elem in self.caret_symbols:
			r = re.compile(elem)
			self.serve_string = r.sub(' ', self.serve_string)


	def __remove_tag(self):

		r = re.compile(self.any_tag)
		self.serve_string = r.sub('', self.serve_string)

	def __remove_html(self):

		r = re.compile(self.any_html)
		self.serve_string = r.sub('', self.serve_string)

	def __remove_time(self):
		r = re.compile(self.any_time)
		self.serve_string = r.sub('', self.serve_string)

	def __remove_year(self):
		r = re.compile(self.any_year)
		self.serve_string = r.sub('', self.serve_string)

	def __remove_slash(self):

		for elem in self.any_slashes:
			r = re.compile(elem)
			self.serve_string = r.sub('', self.serve_string)

	def __remove_other_symbols(self):

		for elem in self.symbols:
			r = re.compile(elem)
			self.serve_string = r.sub('', self.serve_string)

	def __replace_multi_spaces(self):

		r = re.compile(self.multi_spaces)
		self.serve_string = r.sub(' ', self.serve_string)

	def __replace_some_cyrillic(self):

		for sour, dest in self.some_cyrillic.items():
			r = re.compile(sour)
			self.serve_string = r.sub(dest, self.serve_string)

	def start(self, string):
		self.serve_string = string

		for func in self.funcs:
			func()

		return self.serve_string