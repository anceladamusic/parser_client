from django.conf.urls import url
from .ajax import Ajax

urlpatterns = [
    url(r'^ajax/$', Ajax().make_ajax, name='ajax'),
]