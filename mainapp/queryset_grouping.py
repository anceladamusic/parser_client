import itertools
import time
from datetime import timedelta

from mainapp.base import Base as MainappBase

class QuerysetGrouping():

	def __init__(self, **kwargs):
		self.queryset = kwargs['queryset']
		self.group_by = kwargs['group_by']
		self.min = kwargs['min']
		self.max = kwargs['max']
		self.object_id= kwargs['object_id']
		self.object_type = kwargs['object_type']
		self.unique_type = kwargs['unique_type']

	def __get_hour_ranges(self):

		delta = self.max - self.min

		result = []

		for hour in range(0, 24, 1):

			start = self.min + timedelta(hours=hour)

			end = start + timedelta(minutes=59, seconds=59)

			result.append([start, end])

		return result

	def get_date_ranges(self):

		delta = self.max - self.min

		result = []

		for day in range(0, delta.days+1, 1):

			start = self.min + timedelta(days=day)

			end = start + timedelta(days=1) - timedelta(seconds=1)

			result.append([start, end])

		return result

	def get_week_ranges(self):
		
		delta = self.max - self.min

		result = []

		for week in range(0, delta.days + 1, 7):

			start = self.min + timedelta(days=week)

			end = start + timedelta(days=7)-timedelta(seconds=1)

			result.append([start, end])

		return result

	def group(self):

		# группировка по неделям
		if self.group_by == 'week':

			week_ranges = self.get_week_ranges()

			result = []

			for week_range in week_ranges:
				
				str_week_range = '{0} - {1}'.format(
					week_range[0].strftime('%a %d %b'),
					week_range[1].strftime('%a %d %b'),
				)
				
				iso_week_range = [week.isoformat() for week in week_range]
				
				result += [
					[str_week_range, len([line for line in self.queryset if week_range[0] < \
					line['date'] < week_range[1]]), {
						'key': str_week_range,
						'range': iso_week_range,
						'type': self.group_by,
						'object_id': self.object_id,
						'object_type': self.object_type,
						'unique_type': self.unique_type,
					}]
				]

		# группировка по дням
		elif self.group_by == 'day':

			date_ranges = self.get_date_ranges()

			result = []

			for date_range in date_ranges:
				
				iso_date_range = [date.isoformat() for date in date_range]

				result += [
					[date_range[0].strftime('%a %d %b'), len([line for line in self.queryset \
					if date_range[0] < line['date'] < date_range[1]]), {
						'key': date_range[0].strftime('%a %d %b'),
						'range': iso_date_range,
						'type': self.group_by,
						'object_id': self.object_id,
						'object_type': self.object_type,
						'unique_type': self.unique_type,
					}]
				]

		# группировка по часам
		elif self.group_by == 'hour':

			hour_ranges = self.__get_hour_ranges()

			result = []

			for hour_range in hour_ranges:
				
				iso_hour_range = [hour.isoformat() for hour in hour_range]

				result += [
					[hour_range[0].strftime('%H:%M %a %d %b '), len([line for line in self.queryset \
					if hour_range[0] < line['date'] < hour_range[1]]), {
						'key': hour_range[0].strftime('%H:%M %a %d %b'),
						'range': iso_hour_range,
						'type': self.group_by,
						'object_id': self.object_id,
						'object_type': self.object_type,
						'unique_type': self.unique_type,
					}]
				]

		return result