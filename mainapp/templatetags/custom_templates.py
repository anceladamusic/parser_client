import time
import pytz
import datetime
from datetime import timezone

from django import template
from django.conf import settings
from django.utils import timezone

django_timezone = settings.TIME_ZONE
timezone.activate(pytz.timezone(django_timezone))
current_tz = timezone.get_current_timezone()

register = template.Library()

@register.filter(is_safe=True)
def label_with_classes(value, arg):

	return value.label_tag(attrs={'class': arg})

@register.filter()
def unixtime_to_datetime(value):
	# return datetime.datetime.fromtimestamp((value-3*3600000)/1000).strftime('%d-%m-%Y')
	return datetime.datetime.fromtimestamp(value/1000).strftime('%d-%m-%Y')

@register.filter()
def date_to_string(value):
	return value.strftime('%d-%m-%Y')