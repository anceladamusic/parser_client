from django.template.loader import render_to_string

class TreeElems():

	def __init__(self):
		pass

	def build_custom_tree(self, elements):

		for elem in elements:
			elem['html'] = render_to_string('content/tree/tree_elem.html', {
				'elem': elem,
			})

		return render_to_string('content/tree/tree_elems.html', {
			'tree_elements': elements,	
		})


	def build_tree_by_element(self, model, element):

		level = element['level']

		elements = [elem.as_dict() for elem in model.objects.all()]

		tree = self.__build_tree(elements, element, [self.__get_fields(element)])

		# реверсируем список
		tree = tree[::-1]

		# добавляем в начало ссылку на главную
		tree.insert(0, self.__mainpage_elem())

		# строим ссылки
		self.__build_url(tree)

		# строим порядковые номера
		self.__add_no(tree)

		for elem in tree:
			elem['html'] = render_to_string('content/tree/tree_elem.html', {
				'elem': elem,
			})

		return render_to_string('content/tree/tree_elems.html', {
			'tree_elements': tree,
		})


	def __build_tree(self, elements, element, result):
		if element['parent_id']:

			elem = [elem for elem in elements if elem['id'] == element['parent_id']]

			if elem:

				result.append(self.__get_fields(elem[0]))

				return self.__build_tree(elements, elem[0], result)
		else:
			return result

	def __get_fields(self, element):
		return {
			'name': element['name'],
			'model_name': element['model_name'],
			'id': element['id'],
			'level': element['level'],
		}

	def __mainpage_elem(self):
		return {
			'name': 'Главная',
			'model_name': None,
			'id': None,
			'level': None,
		}

	def __build_url(self, tree):
		for elem in tree:
			if elem['model_name']:

				elem['url'] = '/{0}/{1}/'.format(elem['model_name'], elem['id'])

			else:
				elem['url'] = '/'

	def __add_no(self, tree):
		for key, value in enumerate(tree):
			tree[key]['no'] = key+1