import threading
import redis
import datetime
import pickle
import asyncio
import aioredis

from functools import partial

class RedisReader():
	
	def __init__(self):
		self.r = redis.StrictRedis('localhost')
		self.keys = self.r.keys()

	async def produce(self, queue, redis_keys_loop):
		
		for key in self.keys:
			
			conn = await aioredis.create_connection('redis://localhost', loop=redis_keys_loop)
			
			value = await conn.execute('get', key)
			
			await queue.put(value)
			
			conn.close()
			
			await conn.wait_closed()
			
		await queue.put(None)
		
	async def consume(self, queue):
		
		while True:
			
			value = await queue.get()
			
			if value:
				
				print (len(pickle.loads(value)))
				
			else:
				
				break
		
	def run(self):

		redis_keys_loop = asyncio.new_event_loop()

		asyncio.set_event_loop(redis_keys_loop)
		
		queue = asyncio.Queue(loop = redis_keys_loop)
		
		producer_coro = self.produce(queue, redis_keys_loop)
		
		consumer_coro = self.consume(queue)
			
		redis_keys_loop.run_until_complete(asyncio.gather(producer_coro, consumer_coro))
		
		redis_keys_loop.close()
		
start = datetime.datetime.now()

redis_reader = RedisReader()

redis_reader.run()

print (datetime.datetime.now() - start)