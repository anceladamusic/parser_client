import uuid
import ipdb
import json
import threading

from django.http import JsonResponse
from dateutil.parser import parse

from diagram.dynamic import Dynamic
from campaign.user_campaign import UserCampaign
from mainapp.base import Base as MainappBase
from campaign.models import Campaign
from campaign.user_campaign import UserCampaign
from diagram.dynamic_period_parser import DynamicPeriodParser
from pubs.pubs import Pubs
from remote.messages import get_messages

class Ajax():

	def __init__(self):
		pass

	def read_ajax(self, request):
		
		if request.method == 'POST':
			
			string = json.loads(request.body.decode('utf-8'))

			if string['method'] == 'get_list_of_pubs':
				
				page_uuid = string['page_uuid']
				
				user_id = request.session['client_id']

				session_key = request.session.session_key
				
				unique_type = string['unique_type']
				
				range_type = string['range_type']
				
				range = [MainappBase().datetime_localize(parse(range).replace(tzinfo=None)
					) for range in string['range']]
				
				date_to = range[1]
				
				date_from = range[0]
				
				dynamic_period_object = DynamicPeriodParser(**{
					'datetime_from': date_from,
					'datetime_to': date_to,
					'period_keyword': 'custom',
					'include_retro': False,
				})
				
				dynamic_period_object.parse()
				
				object_id = string['object_id']
				
				object_type = string['object_type']
				
				user_campaign = UserCampaign()
					
				result = user_campaign.build_point_data(page_uuid, user_id, session_key, dynamic_period_object, [
					'text_poses', 'title_poses'], ['id', 'date'], unique_type, object_id, True)
				
				keywords_lists = result['keywords_lists']
				
				pubs = result['pubs']
				
				pubs_object = Pubs(**{
					'keywords_lists': keywords_lists,
					'pubs': pubs,
				})
				
				pubs_object.run()
				
				package = {
					'page_uuid': page_uuid,
					'table': pubs_object.html,
				}
				
				get_messages().make_queue(session_key, package, 'pubs_list')
				
				return JsonResponse({
					'string': 'ok',
				})