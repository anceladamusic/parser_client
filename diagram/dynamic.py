import locale
import ipdb

from datetime import timedelta, datetime

from mainapp.base import Base as MainappBase

locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')

class Dynamic():

	def __init__(self, **kwargs):
		self.page_uuid = kwargs['page_uuid']
		self.user_id = kwargs['user_id']
		self.session_key = kwargs['session_key']

		self.id = kwargs['id']
		self.type = kwargs['type']
		self.name = kwargs['name']
		self.keywords_lists = kwargs['keywords_lists']

		# dynamic_period object
		self.dynamic_period_object = kwargs['dynamic_period_object']
		self.point_load = kwargs['point_load']

		self.date_from = self.dynamic_period_object.date_from
		self.date_to = self.dynamic_period_object.date_to
		self.date_ranges_lists = [
			MainappBase().daterange(**{
				'start_date': self.date_from[0],
				'end_date':	self.date_to[0],
			}),
			MainappBase().daterange(**{
				'start_date': self.date_from[1],
				'end_date':	self.date_to[1],
			})
		]
		
		self.search_fields = ['text_poses', 'title_poses']

		self.title = 'Динамика публикаций "{0}" с {1} по {2}'.format(
			self.name,
			self.date_from[1].strftime('%a %d %b %Y'),
			self.date_to[1].strftime('%a %d %b %Y'),
		)
		self.subtitle = 'В сравнении с динамикой дней с {0} по {1}'.format(
			self.date_from[0].strftime('%a %d %b %Y'),
			self.date_to[0].strftime('%a %d %b %Y'),
		)
		self.legend = [
			'{0} - {1}'.format(self.date_from[0].strftime('%a %d %b'), self.date_to[0].strftime('%a %d %b')),
			'{0} - {1}'.format(self.date_from[1].strftime('%a %d %b'), self.date_to[1].strftime('%a %d %b')),
		]

		self.categories = self.__build_categories()

		self.group_by = self.dynamic_period_object.step_type

		# результат выборки
		self.result = [
			{
				'all': [],
				'unique': [],
				'copy': [],
				'reprint': [],
				'incorrect': [],
			},
			{
				'all': [],
				'unique': [],
				'copy': [],
				'reprint': [],
				'incorrect': [],
			},
		]
		self.result_count = [
			{
				'all': None,
				'unique': None,
				'copy': None,
				'reprint': None,
				'incorrect': None,
			},
			{
				'all': None,
				'unique': None,
				'copy': None,
				'reprint': None,
				'incorrect': None,
			}
		]
		# результат группировки
		self.series = [
			{
				'all': [],
				'unique': [],
				'copy': [],
				'reprint': [],
				'incorrect': [],
			},
			{
				'all': [],
				'unique': [],
				'copy': [],
				'reprint': [],
				'incorrect': [],
			},
		]

	# построить категории
	def __build_categories(self):
		
		# если за сегодня
		if self.dynamic_period_object.period_keyword == 'today':
			
			self.title = 'Динамика публикаций "{0}" за сегодня'.format(
			self.name)

			self.subtitle = 'В сравнении с динамикой за вчера'

			result = [group_date for group_date in MainappBase().hourrange(
				self.date_from[1], self.date_to[1])]

			result = ['{0}'.format(line.strftime('%H:%M')) for line in result]
			
		# если за вчера
		elif self.dynamic_period_object.period_keyword == 'yesterday':

			self.title = 'Динамика публикаций "{0}" за вчера'.format(
			self.name)

			self.subtitle = 'В сравнении с динамикой за позавчера'


			result = [group_date for group_date in MainappBase().hourrange(
				self.date_from[1], self.date_to[1])]

			result = ['{0}'.format(line.strftime('%H:%M')) for line in result]
			
		# если 28 дней
		elif self.dynamic_period_object.period_keyword == 'last 28 days':
			
			result = [group_date for group_date in MainappBase().weekrange(
				self.date_from[1], self.date_to[1])]

			result = ['{0} - {1}'.format(line.strftime('%a %d %b'), (line + timedelta(
				days=6)).strftime('%a %d %b')) for line in result]
			
		# если 35 дней
		elif self.dynamic_period_object.period_keyword == 'last 35 days':
			
			result = [group_date for group_date in MainappBase().weekrange(
				self.date_from[1], self.date_to[1])]

			result = ['{0} - {1}'.format(line.strftime('%a %d %b'), (line + timedelta(
				days=6)).strftime('%a %d %b')) for line in result]
			
		# если 7
		elif self.dynamic_period_object.period_keyword == 'last 7 days':
			
			result = [date for date in MainappBase().daterange(
				self.date_from[1], self.date_to[1])]
			
			result = ['{0}'.format(line.strftime('%a %d %b')) for line in result]
			
		# если 14 дней
		elif self.dynamic_period_object.period_keyword == 'last 14 days':
			
			result = [date for date in MainappBase().daterange(
				self.date_from[1], self.date_to[1])]
			
			result = ['{0}'.format(line.strftime('%a %d %b')) for line in result]
			
		# если за квартал
		elif self.dynamic_period_object.period_keyword == 'quarter begin' or \
			self.dynamic_period_object.period_keyword  == 'current year' or self.dynamic_period_object.period_keyword == 'custom':
			
			if self.dynamic_period_object.step_type == 'day':
				
				result = [date for date in MainappBase().daterange(
				self.date_from[1], self.date_to[1])]
			
				result = ['{0}'.format(line.strftime('%a %d %b')) for line in result]
				
			if self.dynamic_period_object.step_type == 'week':
				
				result = [group_date for group_date in MainappBase().weekrange(
				self.date_from[1], self.date_to[1])]

				result = ['{0} - {1}'.format(line.strftime('%a %d %b'), (line + timedelta(
					days=6)).strftime('%a %d %b')) for line in result]
				
			if self.dynamic_period_object.step_type == 'hour':
				
				result = [group_date for group_date in MainappBase().hourrange(
				self.date_from[1], self.date_to[1])]

				result = ['{0}'.format(line.strftime('%H:%M')) for line in result]
		return result