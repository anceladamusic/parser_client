import datetime
import copy
import ipdb
import time
import threading
import pickle
import asyncio
import aioredis

from uuid import uuid1
from django.template.loader import render_to_string

from mainapp.queryset_grouping import QuerysetGrouping
from remote.messages import get_messages
from remote.active_users import get_page
from canonizator.models import NormalizePublication

class Query():

	def __init__(self, **kwargs):
		self.keys = kwargs['keys']
		self.dynamic_objects = self.__sort_dynamic_objects(kwargs['dynamic_objects'])
		self.query_fields = kwargs['query_fields']
		self.returning_fields = kwargs['returning_fields']
		self.group_by = kwargs['group_by']
		self.unique_type = kwargs['unique_type']
		self.dynamic_object_id = kwargs['dynamic_object_id']
		self.point_load = kwargs['point_load']
		self.dynamic_period_object = kwargs['dynamic_period_object']
		self.page = None


	def __sort_dynamic_objects(self, dynamic_objects):

		all_campaign = [dynamic_object for dynamic_object in dynamic_objects \
		 if dynamic_object.type=='all_campaign']

		campaigns = [dynamic_object for dynamic_object in dynamic_objects \
		 if dynamic_object.type=='campaign']

		keywords = [dynamic_object for dynamic_object in dynamic_objects \
		 if dynamic_object.type=='keyword']

		return all_campaign + campaigns + keywords
	
	# получить keywords и pubs
	def __get_keywords_n_pubs(self):
		
		dynamic_object = [dynamic_object for dynamic_object in self.dynamic_objects \
			if dynamic_object.id == self.dynamic_object_id][0]
		
		pubs = []
		
		for result_key, result_value in enumerate(dynamic_object.result):
			
			pubs += [value for key, value in result_value.items() if key == self.unique_type][0]
			
		pubs = [pub for pub in pubs if pub['date'] > self.dynamic_period_object.datetime_from and pub['date'] < \
			self.dynamic_period_object.datetime_to]
		
		return {
			'keywords_lists': dynamic_object.keywords_lists,
			'pubs': pubs,
		}
	
	# группировка и отправка сообщений слушателю
	def __group_n_message(self):
		
		# to active_users page
		campaign_dynamic = []

		for dynamic_object in self.dynamic_objects:

			for result_key, result_value in enumerate(dynamic_object.result):

				for key, value in result_value.items():

					count_groups = QuerysetGrouping(**{
						'queryset': sorted(value, key=lambda k: k['date']),
						'group_by': dynamic_object.group_by,
						'min': dynamic_object.date_from[result_key],
						'max': dynamic_object.date_to[result_key],
						'object_id': dynamic_object.id,
						'object_type': dynamic_object.type,
						'unique_type': key,
					}).group()

					dynamic_object.series[result_key][key] += [[
						group[0],
						group[1],
						group[2],
					] for group in count_groups]

					# заполняем количество
					dynamic_object.result_count[result_key][key] = len(dynamic_object.result[result_key][key])

				package = {
					'page_uuid': dynamic_object.page_uuid,
					'id': dynamic_object.id,
					'type': dynamic_object.type,
					'series': dynamic_object.series,
					'count': dynamic_object.result_count,
					'categories': dynamic_object.categories,
					'title': dynamic_object.title,
					'subtitle': dynamic_object.subtitle,
					'legend': dynamic_object.legend,
				}
				
			# to active users list add
			campaign_dynamic.append(package)

			get_messages().make_queue(dynamic_object.session_key, package, 'dynamic_diagram_package')
		
		# update active_users page
		self.page['data'] = campaign_dynamic

	def __keywords_in_pub(self, keywords, pub, date_range_key, dynamic_object):

		if [field for field in [field for field in self.query_fields if [
			word for word in keywords if word['pos'] in pub[field]]
			] if len([word for word in keywords if word['crc32'] in pub[field][
			word['pos']]]) == len(keywords)]:

			dynamic_object.result[date_range_key]['all'] += [pub]

			if pub['status_text'] == 'уникальная':
				dynamic_object.result[date_range_key]['unique'] += [pub]

			elif pub['status_text'] == 'скопированная':
				dynamic_object.result[date_range_key]['copy'] += [pub]

			elif pub['status_text'] == 'перепечатанная':
				dynamic_object.result[date_range_key]['reprint'] += [pub]

			return True

	def __pop_from_pubs(self, dynamic_object, pubs, key):

		for date_range_key, date_range_list in enumerate(dynamic_object.date_ranges_lists):

			if [date_range for date_range in date_range_list if date_range.date() == key[
				'packet']]:

				for keywords in dynamic_object.keywords_lists:

					pubs[:] = (pub for pub in pubs if not self.__keywords_in_pub(
						keywords, pub, date_range_key, dynamic_object))
	
	async def parse_packet(self, packet):
		
		copy_packet = copy.copy(packet['packet'])

		all_campaign_copy = copy.copy(packet['packet'])

		for dynamic_object in self.dynamic_objects:

			if dynamic_object.type == 'all_campaign':
				
				self.__pop_from_pubs(dynamic_object, all_campaign_copy, packet['key'])

			if dynamic_object.type == 'campaign':
				
				self.__pop_from_pubs(dynamic_object, copy_packet, packet['key'])

			if dynamic_object.type == 'keyword':

				self.__pop_from_pubs(dynamic_object, packet['packet'], packet['key'])

		del copy_packet[:]

		del all_campaign_copy[:]

	async def consume(self, queue):
		
		while True:
			
			packet = await queue.get()
			
			if packet:
				
				await self.parse_packet(packet)
				
			else:
				
				break

	async def produce(self, queue, loop):
		
		for key in self.keys:
			
			conn = await aioredis.create_connection('redis://localhost', loop=loop)
	
			packet = await conn.execute('get', pickle.dumps(key))
			
			await queue.put({
				'key': key,
				'packet': pickle.loads(packet),
			})
	
			conn.close()
	
			await conn.wait_closed()
			
		await queue.put(None)
	
	def run(self):
		
		# убираем лишние ключи из выборки
		if self.dynamic_objects:

			matched = []

			for date_range_list in self.dynamic_objects[0].date_ranges_lists:

				matched += [key for key in self.keys if key['packet'] in [date_range.date(
					) for date_range in date_range_list]]

			self.keys = matched
		
		
		# ждем, когда можно проводить загрузку
		while not get_page(
			self.dynamic_objects[0].user_id,
			self.dynamic_objects[0].session_key,
			self.dynamic_objects[0].page_uuid,
		):
		
			time.sleep(0.5)
			
		while not get_page(
			self.dynamic_objects[0].user_id,
			self.dynamic_objects[0].session_key,
			self.dynamic_objects[0].page_uuid,
		)['can_receive_data']:
			
			time.sleep(0.5)
			
		self.page = get_page(
			self.dynamic_objects[0].user_id,
			self.dynamic_objects[0].session_key,
			self.dynamic_objects[0].page_uuid,
		)
			
		print ('--------------------can receive data')

		# начинаем загрузку
		start = datetime.datetime.now()

		loop = asyncio.new_event_loop()

		asyncio.set_event_loop(loop)
		
		queue = asyncio.Queue(loop = loop)
		
		producer_coro = self.produce(queue, loop)
		
		consumer_coro = self.consume(queue)

		loop.run_until_complete(asyncio.gather(producer_coro, consumer_coro))

		loop.close()

		# если за конкретный point
		result = None
		
		if self.point_load:
			
			return self.__get_keywords_n_pubs()
			
			
		# группируем и направляем сообщения пользователю
		else:
			
			self.__group_n_message()
			

		print (datetime.datetime.now() - start)
		print ('------------end query')
		
		return result