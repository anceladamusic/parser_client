import ipdb
import datetime

from datetime import timedelta

from mainapp.base import Base as MainappBase


class DynamicPeriodParser():
    
    def __init__(self, **kwargs):
        
        if 'period_keyword' in kwargs:
            self.period_keyword = kwargs['period_keyword']
            
        self.datetime_to = kwargs['datetime_to']
        
        if 'datetime_from' in kwargs:
            self.datetime_from = kwargs['datetime_from']
        else:
            self.datetime_from = self.datetime_to - timedelta(days=7)
            
        self.include_retro = kwargs['include_retro']
        
        self.step_type = 'day'
        self.date_from = []
        self.date_to = []
    
    # look up date from and to from now by offset
    def __get_dates(self, now, offset_days):
        
        date_to = now.replace(hour=23, minute=59, second=59)
        
        date_from = (date_to - timedelta(days=offset_days)).replace(
            hour=0, minute=0, second=0)
        
        return {
            'date_from': date_from,
            'date_to': date_to,
        }
 
    # возвращает период запроса и тип шага   
    def parse(self):
        
        if self.period_keyword == 'today':
            
            dates = self.__get_dates(self.datetime_to, 0)
            
            dates_retro = self.__get_dates(self.datetime_to-timedelta(days=1), 0)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            self.step_type = 'hour'
            
        elif self.period_keyword == 'yesterday':
            
            self.datetime_to -= timedelta(days=1)
            
            dates = self.__get_dates(self.datetime_to, 0)
            
            dates_retro = self.__get_dates(self.datetime_to-timedelta(days=1), 0)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            self.step_type = 'hour'
            
        elif self.period_keyword == 'last 7 days':
            
            offset_days = 7
            
            dates = self.__get_dates(self.datetime_to, offset_days - 1)
            
            dates_retro = self.__get_dates(self.datetime_to - timedelta(days=offset_days), offset_days - 1)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            self.step_type = 'day'
            
        elif self.period_keyword == 'last 14 days':
            
            offset_days = 14
            
            dates = self.__get_dates(self.datetime_to, offset_days - 1)
            
            dates_retro = self.__get_dates(self.datetime_to - timedelta(days=offset_days), offset_days - 1)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            self.step_type = 'day'
        
        elif self.period_keyword == 'last 28 days':
            
            offset_days = 28
            
            dates = self.__get_dates(self.datetime_to, offset_days - 1)
            
            dates_retro = self.__get_dates(self.datetime_to - timedelta(days=offset_days), offset_days - 1)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            self.step_type = 'week'
        
        elif self.period_keyword == 'last 35 days':
            
            offset_days = 35
            
            dates = self.__get_dates(self.datetime_to, offset_days - 1)
            
            dates_retro = self.__get_dates(self.datetime_to - timedelta(days=offset_days), offset_days - 1)

            self.date_from += [dates_retro['date_from'], dates['date_from']]    
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            self.step_type = 'week'
        
        elif self.period_keyword == 'quarter begin':
            
            quarter_first_day = MainappBase().get_quarter_first_day(self.datetime_to)
            
            offset_days = (self.datetime_to - quarter_first_day).days
            
            if offset_days % 7:
            
                self.datetime_to -= timedelta(days = offset_days % 7)
                
                offset_days = (self.datetime_to - quarter_first_day).days
                
            dates = self.__get_dates(self.datetime_to, offset_days)
            
            dates_retro = self.__get_dates(dates['date_from']-timedelta(days=7), offset_days)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], MainappBase().datetime_localize(datetime.datetime.now())]
            
            if offset_days >= 28:
                
                self.step_type = 'week'
                
            elif offset_days == 1:
                
                self.step_type = 'hour'
                
            else:
                
                self.step_type = 'day'
                
        elif self.period_keyword == 'current year':
            
            year_first_day = MainappBase().get_year_first_day(self.datetime_to)
            
            offset_days = (self.datetime_to - year_first_day).days
            
            if offset_days % 7:
            
                self.datetime_to -= timedelta(days = offset_days % 7)
                
                offset_days = (self.datetime_to - year_first_day).days
            
            dates = self.__get_dates(self.datetime_to, offset_days)

            dates_retro = self.__get_dates(dates['date_from'] - timedelta(days=7), offset_days)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], MainappBase().datetime_localize(datetime.datetime.now())]
            
            if offset_days >= 28:
                
                self.step_type = 'week'
                
            elif offset_days == 1:
                
                self.step_type = 'hour'
                
            else:
                
                self.step_type = 'day'
                
        elif self.period_keyword == 'custom':
            
            offset_days = (self.datetime_to - self.datetime_from).days
            
            dates = self.__get_dates(self.datetime_to, offset_days)
            
            if offset_days % 7:
                
                date_to_retro = self.datetime_to - timedelta(days=offset_days) - timedelta(days = 7 - offset_days % 7)
           
                dates_retro = self.__get_dates(date_to_retro, offset_days)
                
            else:
                
                dates_retro = self.__get_dates(self.datetime_to - timedelta(days=offset_days), offset_days - 1)
            
            self.date_from += [dates_retro['date_from'], dates['date_from']]
            self.date_to += [dates_retro['date_to'], dates['date_to']]
            
            if offset_days >= 28:
                
                self.step_type = 'week'
                
            elif offset_days == 1:
                
                self.step_type = 'hour'
                
            else:
                
                self.step_type = 'day'