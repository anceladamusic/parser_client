import datetime

from django.utils import timezone

from django import forms
from mainapp.base import Base as MainappBase

def get_now():
    
    now = MainappBase().datetime_localize(datetime.datetime.now())
    
    return now.strftime('%d-%m-%Y')

class UsersPeriod(forms.Form):
    
    date_from = forms.CharField(label='Начать с', initial=get_now, required=True, max_length=100)
    date_to = forms.CharField(label='Окончить по', initial=get_now, required=True, max_length=100)