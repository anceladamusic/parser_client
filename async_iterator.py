import timeit
import ipdb
import asyncio
import functools
import aioredis

class reader():
	
	def __init__(self):
		pass
	
	async def slow_operation(self, future):
		
		await asyncio.sleep(1)
		
		future.set_result('Future is done!')
		
	# генератор
	def gen(self):
		for i in range(0, 10):
			yield i

	def start(self):
		
		loop = asyncio.new_event_loop()
		
		asyncio.set_event_loop(loop)
		
		future = asyncio.Future()

		asyncio.ensure_future(self.slow_operation(future))
		
		loop.run_until_complete(future)
		
		print (future.result())
		
		print ('-----------------end future result')
		
		loop.close()

# reader().start()
		
print (timeit.timeit('reader().start()', globals=globals(), number=1))