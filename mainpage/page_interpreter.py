from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404

from oauth.properties import Properties
from mainapp.google_objects import WebsiteInfo, ServiceInfo, Organization
from interface_top.topbar import Topbar as InterfaceTopbar
from .models import Topbar

class PageInterpreter():

	def __init__(self):
		self.google_objects = [
			WebsiteInfo(),
			ServiceInfo(),
			Organization(),
		]

	def build_login(self, request):

		elements = Topbar.objects.all()

		topbar = InterfaceTopbar(**{
			'request': request,
			'element_html': 'mainpage/topbar_element.html',
			'elements': [elem.as_dict() for elem in elements],
			'selected': {},
		}).build()

		meta = {
			'meta_title': 'вход в систему',
			'meta_description': 'Вход в систему',
		}

		prop = Properties().auth_settings

		html = render_to_string('login.html', {
			'vk': prop['vk'],
			'fb': prop['fb'],
			'gp': prop['gp'],
			'ya': prop['ya'],
		})

		return {
			'google_objects': self.google_objects,
			'html': html,
			'meta': meta,
			'topbar': topbar,
		}

	def build_mainpage(self, request):
		
		element = get_object_or_404(Topbar, id=1)

		elements = Topbar.objects.all()

		topbar = InterfaceTopbar(**{
			'request': request,
			'element_html': 'mainpage/topbar_element.html',
			'elements': [elem.as_dict() for elem in elements],
			'selected': element.as_dict(),
		}).build()

		meta = {
			'meta_title': element.as_dict()['short_description'],
			'meta_description': element.as_dict()['description'],
		}

		html = render_to_string('content.html', {
			'element': element.as_dict(),
		})

		return {
			'google_objects': self.google_objects,
			'html': html,
			'meta': meta,
			'topbar': topbar,
		}

	def build_category(self, request, topbar_category=None):

		element = get_object_or_404(Topbar, topbar_category=topbar_category)

		elements = Topbar.objects.all()

		topbar = InterfaceTopbar(**{
			'request': request,
			'element_html': 'mainpage/topbar_element.html',
			'elements': [elem.as_dict() for elem in elements],
			'selected': element.as_dict(),
		}).build()

		meta = {
			'meta_title': element.as_dict()['short_description'],
			'meta_description': element.as_dict()['description'],
		}

		html = render_to_string('content.html', {
			'element': element.as_dict(),
		})

		return {
			'google_objects': self.google_objects,
			'html': html,
			'meta': meta,
			'topbar': topbar,
		}