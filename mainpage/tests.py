from django.test import Client, TestCase
from django.urls import reverse

from mainpage.models import Topbar

class MainpageTest(TestCase):

	fixtures = ['mainpage.json']

	def setUp(self):
		self.c = Client(HTTP_USER_AGENT='Mozilla/5.0')
		self.topbar = Topbar.objects.all()

	def login(self):

		response = self.c.get(reverse('mainpage:login'))

		self.assertEqual(response.status_code, 200)

	def logout(self):

		response = self.c.get(reverse('mainpage:logout'))

		self.assertEqual(response.status_code, 302)

	def category(self):

		response = self.c.get(reverse('mainpage:index'))

		self.assertEqual(response.status_code, 200)

		for item in self.topbar:

			response = self.c.get(
				reverse('mainpage:topbar_category', kwargs={'topbar_category': item.topbar_category}))

			self.assertEqual(response.status_code, 200)