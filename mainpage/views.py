from uuid import uuid1
from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect

from .page_interpreter import PageInterpreter
from mainapp.base import Base
from jsonld.views import *

def mainpage(request):
	
	if not request.session.session_key:
		request.session.save()

	session = Base().check_authorize(request)

	if session['client_id']:

		return HttpResponseRedirect(reverse(
			'campaign:campaign', kwargs={}))

	page = PageInterpreter().build_mainpage(request)

	content = page['html']

	google_objects = page['google_objects']

	jsonld = to_jsonld(google_objects)

	return render(request, 'main.html', {
		'page_uuid': str(uuid1()),
		'page_type': 'mainpage',
		'topbar': page['topbar'],
		'content': content,
		'jsonld' : jsonld,
		'session': session,
		'meta': page['meta'],
	})

def topbar_category(request, topbar_category):

	if not request.session.session_key:
		request.session.save()

	session = Base().check_authorize(request)

	if session['client_id']:

		return HttpResponseRedirect(reverse(
			'campaign:campaign', kwargs={}))

	page = PageInterpreter().build_category(request, topbar_category)

	content = page['html']

	google_objects = page['google_objects']

	jsonld = to_jsonld(google_objects)

	return render(request, 'main.html', {
		'page_uuid': str(uuid1()),
		'page_type': 'topbar_category',
		'topbar': page['topbar'],
		'content': content,
		'jsonld' : jsonld,
		'session': session,
		'meta': page['meta'],
	})

def login(request):

	if not request.session.session_key:
		request.session.save()

	session = Base().check_authorize(request)

	if session['client_id']:

		return HttpResponseRedirect(reverse(
			'campaign:campaign', kwargs={}))

	page = PageInterpreter().build_login(request)

	content = page['html']

	google_objects = page['google_objects']

	jsonld = to_jsonld(google_objects)

	return render(request, 'main.html', {
		'page_uuid': str(uuid1()),
		'page_type': 'login',
		'topbar': page['topbar'],
		'content': content,
		'jsonld' : jsonld,
		'session': session,
		'meta': page['meta'],
	})	

def logout(request):

	if 'authorized' in request.session:
		
		del request.session['authorized']

		del request.session['client_id']

		del request.session['name']

	return HttpResponseRedirect(reverse('mainpage:index'))