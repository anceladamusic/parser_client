from django.apps import apps
from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin
from .models import *

# class UnitColorChildrenInline(admin.TabularInline):
# 	model = UnitColorChildren


class Inlines():

	def __init__(self):
		self.model = {
			# 'mainpage.unit': [
			# 	UnitColorChildrenInline,
			# ],
		}

app_models = apps.get_app_config('mainpage').get_models()

for model in app_models:

	if model.get_type() == 'MPTTModel':

		class ModelAdmin(DjangoMpttAdmin):

			if str(model._meta) in Inlines().model:

				inlines = Inlines().model[str(model._meta)]
				
		admin.site.register(model, ModelAdmin)

	else:

		class ModelAdmin(admin.ModelAdmin):
			list_display = [field.name for field in model._meta.fields]

			if str(model._meta) in Inlines().model:

				inlines = Inlines().model[str(model._meta)]

		admin.site.register(model, ModelAdmin)