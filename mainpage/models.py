from mptt.models import MPTTModel, TreeForeignKey

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_mysql.models import Model
from django.db import models

from interface_top.models import AbstractTop

class Topbar(AbstractTop):

	class Meta():
		verbose_name = 'Раздел меню главной страницы'
		verbose_name_plural = 'Разделы меню главной страницы'

	text = models.TextField('текст')


	def as_dict(self):

		parent_id = 0

		if self.parent_id:

			parent_id = self.parent_id

		return {
			'id': self.id,
			'name': self.name,
			'description': self.description,
			'short_description': self.short_description,
			'class_name': self.__class__.__name__,
			'parent_id': parent_id,
			'level': self.level,
			'topbar_category': self.topbar_category,
			'text': self.text,
		}