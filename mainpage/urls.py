from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.mainpage, name='index'),
	url(r'^login/$', views.login, name='login'),
	url(r'^logout/$', views.logout, name='logout'),
	url(r'^(?P<topbar_category>[A-z]+)/$', views.topbar_category, name='topbar_category'),
]